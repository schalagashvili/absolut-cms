<?php

use craft\elements\Entry;
use craft\elements\Asset;
use craft\elements\Category;
use craft\elements\Tag;
use craft\elements\MatrixBlock;
use League\Fractal\TransformerAbstract;

class PropertyListTransformer extends TransformerAbstract
{

    public function transform(Entry $entry)
    {
        
        $categories = array_map(function(Category $entry) {
            return [
                'title' => $entry->title,
                'id' => $entry->id
            ];
        }, $entry->categories->all());
    

        $images = array_map(function(Asset $entry) {
            return [
                'id' => $entry->id,
                'url' => $entry->getUrl()
            ];
        }, $entry->images->all());


        $coordinates = array_map(function(MatrixBlock $entry) {
            return [
                'longtitude' => $entry->longtitude,
                'latitude' => $entry->latitude
            ];
        }, $entry->coordinates->all());


        $tags = array_map(function(Tag $entry) {
            return [
                'tags' => $entry->title,
                'id' => $entry->id
            ];
        }, $entry->tags->all());

        return [
            'name' => $entry->propertyName,
            'id' => $entry->id,
            'description' => $entry->description,
            'images' => $images,
            'city' => $entry->city,
            'district' => $entry->district,
            'categories' => $categories,
            'coordinates' => $coordinates,
            'tags' => $tags,
            'location' => $entry->propertyLocation,
            'url' => $entry->propertyURL,
            'number' => $entry->propertyNumber,
            'email' => $entry->propertyEmail,
        ];
    }
}