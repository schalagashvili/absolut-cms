<?php

use craft\elements\Entry;
require_once 'transformers/PropertyListTransformer.php';


return [
    'endpoints' => [
        'properties.json' => function() {
            return [
                'elementType' => Entry::class,
                'elementsPerPage' => 10,
                'criteria' => ['section' => 'properties'],
                'transformer' => new PropertyListTransformer(),
            ];
        },
        'property/<entryId:\d+>.json' => function($entryId) {
            return [
                'elementType' => Entry::class,
                'criteria' => ['id' => $entryId],
                'one' => true,
                'transformer' => new PropertyListTransformer(),
            ];
        },
        'property/title=<slug:{slug}>.json' => function($slug) {
            return [
                'elementType' => Entry::class,
                'criteria' => ['section' => 'properties', 'title' => '*'.$slug.'*'],
                'transformer' => new PropertyListTransformer($slug)
            ];
        },
        'entries-by-category' => function() {
            $categoryIds = Craft::$app->request->get('categoryIds');
            
            $categoryIdsArray = [];
            $array = explode(',', $categoryIds);
            foreach ($array as $value) {
                $categoryIdsArray[] = (int)$value;
            }


            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'relatedTo' => ['targetElement' => $categoryIdsArray],
                ],
                'transformer' => new PropertyListTransformer(),
            ];
        },
        'entries-by-tag' => function() {
            $tagsId = Craft::$app->request->get('categoryIds');
            
            $tagsIdArray = [];
            $array = explode(',', $tagsId);
            foreach ($array as $value) {
                $tagsIdArray[] = (int)$value;
            }

            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'relatedTo' => ['targetElement' => $tagsIdArray],
                ],
                'transformer' => function(Entry $cat) {
                    return [
                        'title' => $cat->title,
                        'id' => $cat->id
                    ];
                },
            ];
        },
    ]
];
