-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: test
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.33-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assetindexdata`
--

DROP TABLE IF EXISTS `assetindexdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  KEY `assetindexdata_volumeId_idx` (`volumeId`),
  CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assetindexdata`
--

LOCK TABLES `assetindexdata` WRITE;
/*!40000 ALTER TABLE `assetindexdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `assetindexdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assets_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `assets_folderId_idx` (`folderId`),
  KEY `assets_volumeId_idx` (`volumeId`),
  CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (14,1,1,'Screenshot_3.png','image',1022,1093,72298,NULL,'2018-07-02 09:30:53','2018-07-02 09:28:22','2018-07-02 10:12:08','e3cba627-5457-4452-b291-f2d668d750f3'),(15,1,1,'Screenshot_2.png','image',571,296,21814,NULL,'2018-07-02 10:12:03','2018-07-02 10:12:03','2018-07-02 10:12:03','80e18d51-2082-4451-bd5a-a839ba21a8f8'),(16,1,1,'Screenshot_4.png','image',93,522,19376,NULL,'2018-07-02 10:15:24','2018-07-02 10:15:24','2018-07-02 10:15:24','59e8ddf8-0793-4e83-884e-e439bef1735b'),(17,1,1,'Screenshot_2_180702_102323.png','image',571,296,21814,NULL,'2018-07-02 10:23:23','2018-07-02 10:23:23','2018-07-02 10:23:23','425f6a78-4f25-47b4-8d99-49bbd572324f'),(18,1,1,'Screenshot_3_180702_102323.png','image',1022,1093,72298,NULL,'2018-07-02 10:23:25','2018-07-02 10:23:25','2018-07-02 10:23:25','40dcd00b-7965-4d5d-8b15-9d7639342506');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransformindex`
--

DROP TABLE IF EXISTS `assettransformindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransformindex`
--

LOCK TABLES `assettransformindex` WRITE;
/*!40000 ALTER TABLE `assettransformindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransforms`
--

DROP TABLE IF EXISTS `assettransforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransforms`
--

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;
/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_idx` (`groupId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (2,1,'2018-07-02 07:22:15','2018-07-02 07:22:15','43388d9a-70e7-475d-b77b-6ed8e755337b'),(3,1,'2018-07-02 07:22:24','2018-07-02 07:22:24','fb2c3fbd-75dc-4ba3-b6d5-429ab64c1e2f'),(4,1,'2018-07-02 07:22:33','2018-07-02 07:22:33','ae777979-c5ca-4715-8cc1-b6f04aa50bc7'),(5,1,'2018-07-02 07:22:39','2018-07-02 07:22:39','06ffac7f-b97a-493c-98a0-60cbdce34ae7');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups`
--

DROP TABLE IF EXISTS `categorygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `categorygroups_handle_unq_idx` (`handle`),
  KEY `categorygroups_structureId_idx` (`structureId`),
  KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups`
--

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;
INSERT INTO `categorygroups` VALUES (1,1,2,'Property Types','propertyTypes','2018-07-02 07:14:24','2018-07-02 07:22:05','e331ad79-e7e5-401a-bdf0-bedb1ebf2b45');
/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups_sites`
--

DROP TABLE IF EXISTS `categorygroups_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  KEY `categorygroups_sites_siteId_idx` (`siteId`),
  CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups_sites`
--

LOCK TABLES `categorygroups_sites` WRITE;
/*!40000 ALTER TABLE `categorygroups_sites` DISABLE KEYS */;
INSERT INTO `categorygroups_sites` VALUES (1,1,1,1,'bar/{slug}','bar/_category','2018-07-02 07:14:24','2018-07-02 07:22:05','b0a108f4-9e71-4cb2-9822-2f4c7b6f5bd2');
/*!40000 ALTER TABLE `categorygroups_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_propertyName` text,
  `field_propertyURL` varchar(255) DEFAULT NULL,
  `field_propertyEmail` varchar(255) DEFAULT NULL,
  `field_propertyNumber` int(10) DEFAULT NULL,
  `field_propertyLocation` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `content_siteId_idx` (`siteId`),
  KEY `content_title_idx` (`title`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,1,1,NULL,'2018-07-02 06:38:30','2018-07-02 06:38:30','d0460f49-37c9-4ab3-a790-7ed38e7a1d7f',NULL,NULL,NULL,NULL,NULL),(2,2,1,'Bar','2018-07-02 07:22:15','2018-07-02 07:22:15','3b6fa810-2a30-49f8-8333-6e28ec6c7d97',NULL,NULL,NULL,NULL,NULL),(3,3,1,'Restaurant','2018-07-02 07:22:24','2018-07-02 07:22:24','4cf03788-0eb1-4fc5-b366-15cd5c9db152',NULL,NULL,NULL,NULL,NULL),(4,4,1,'Hotel','2018-07-02 07:22:33','2018-07-02 07:22:33','6e6338cb-9c4d-49e2-8565-6bcef1771880',NULL,NULL,NULL,NULL,NULL),(5,5,1,'Club','2018-07-02 07:22:39','2018-07-02 07:22:39','d0656b7f-b0b6-47ca-911d-077f094d19f1',NULL,NULL,NULL,NULL,NULL),(6,6,1,'hello','2018-07-02 07:24:01','2018-07-02 07:24:01','269e8930-020c-44e5-9f68-a290582ab65c',NULL,NULL,NULL,NULL,NULL),(7,7,1,'world','2018-07-02 07:24:05','2018-07-02 07:24:05','6b5efb27-f354-40b5-9797-6804c9319fe3',NULL,NULL,NULL,NULL,NULL),(8,8,1,'S','2018-07-02 07:25:06','2018-07-02 07:25:06','4a84a749-7d0d-4ca1-9c28-5b5b38f6b15e',NULL,NULL,NULL,NULL,NULL),(9,9,1,'Spirit','2018-07-02 07:25:14','2018-07-02 07:25:14','128fcaea-5ea4-4389-8158-7e5348d832f0',NULL,NULL,NULL,NULL,NULL),(10,10,1,'Wine','2018-07-02 07:25:16','2018-07-02 07:25:16','2d8b9c77-6f48-440b-aa8f-2801586dfcfe',NULL,NULL,NULL,NULL,NULL),(11,11,1,'Champagne','2018-07-02 07:25:20','2018-07-02 07:25:20','96cb4e08-6008-4bab-be69-4364d4608a65',NULL,NULL,NULL,NULL,NULL),(12,12,1,'Rival Coctail Bar','2018-07-02 07:27:12','2018-07-02 10:36:08','48ef8cbf-558c-4725-8d6b-fecb73fb0768','Rival Coctail Bar','https://website.com','email@gmail.com',2147483647,'Gldanula, Tbilisi, Georgia'),(13,14,1,'Screenshot 3','2018-07-02 09:28:20','2018-07-02 10:12:08','80cfb66b-130e-42f9-9ec9-4a809fcb9cc4',NULL,NULL,NULL,NULL,NULL),(14,15,1,'Screenshot 2','2018-07-02 10:12:03','2018-07-02 10:12:03','1c4fb053-2ca2-4715-9f75-95c9d70ef818',NULL,NULL,NULL,NULL,NULL),(15,16,1,'Screenshot 4','2018-07-02 10:15:24','2018-07-02 10:15:24','ded6b22d-d562-4f4a-8d87-a1f4fe615a4b',NULL,NULL,NULL,NULL,NULL),(16,17,1,'Screenshot 2','2018-07-02 10:23:23','2018-07-02 10:23:23','d9ede8ae-2219-4117-bec5-6ae2c60e8e8c',NULL,NULL,NULL,NULL,NULL),(17,18,1,'Screenshot 3','2018-07-02 10:23:24','2018-07-02 10:23:24','0b0b0e2c-a7b5-4b88-8094-e6dc3e9d2efd',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `craftidtokens`
--

DROP TABLE IF EXISTS `craftidtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craftidtokens_userId_fk` (`userId`),
  CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `craftidtokens`
--

LOCK TABLES `craftidtokens` WRITE;
/*!40000 ALTER TABLE `craftidtokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `craftidtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deprecationerrors`
--

DROP TABLE IF EXISTS `deprecationerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deprecationerrors`
--

LOCK TABLES `deprecationerrors` WRITE;
/*!40000 ALTER TABLE `deprecationerrors` DISABLE KEYS */;
/*!40000 ALTER TABLE `deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementindexsettings`
--

DROP TABLE IF EXISTS `elementindexsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementindexsettings`
--

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` VALUES (1,NULL,'craft\\elements\\User',1,0,'2018-07-02 06:38:30','2018-07-02 06:38:30','ec036719-ecad-46eb-a770-e3ab5ec793e9'),(2,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:15','2018-07-02 07:22:15','86259402-cacf-4769-83a8-9993bb5f8542'),(3,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:24','2018-07-02 07:22:24','00815aa0-abc2-4ba8-9cb9-1bec767732bb'),(4,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:33','2018-07-02 07:22:33','4b2322bd-8106-44cb-b7e5-1194f2046458'),(5,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:39','2018-07-02 07:22:39','2414f776-c76c-4baf-97b6-ebe5db58adc5'),(6,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:24:01','2018-07-02 07:24:01','35e75cb0-ad97-475d-90cc-8fcc244a270e'),(7,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:24:05','2018-07-02 07:24:05','35a2a7c5-12d5-4e46-ae5f-7e1e3a178670'),(8,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:06','2018-07-02 07:25:06','638ba1a3-674c-487a-a79f-d38600c8cfaf'),(9,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:14','2018-07-02 07:25:14','c2fc31cc-ee02-4483-a67c-c4e92fd1ee92'),(10,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:16','2018-07-02 07:25:16','508c607a-5f35-4091-a7df-9d849f8d73b2'),(11,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:20','2018-07-02 07:25:20','24c3c087-c2f2-47f2-b495-c630c3ea9e5d'),(12,4,'craft\\elements\\Entry',1,0,'2018-07-02 07:27:12','2018-07-02 10:36:08','2dd4d5e2-8086-4715-92fa-3a1ef5dcb8df'),(13,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-02 07:27:12','2018-07-02 10:36:08','37e7ad5f-e656-4f42-9491-ddca2d2059b6'),(14,5,'craft\\elements\\Asset',1,0,'2018-07-02 09:28:20','2018-07-02 10:12:08','afa1f552-d1d2-4c31-bae3-367b550a3005'),(15,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:12:03','2018-07-02 10:12:03','7a1685d0-35a8-4306-9d20-c1b16299928d'),(16,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:15:24','2018-07-02 10:15:24','3d97166f-7fe7-4f9e-9d5b-17f12a6aef73'),(17,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:23:23','2018-07-02 10:23:23','a6f5f3c1-647f-4ee8-8f48-aa1e887f92ad'),(18,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:23:24','2018-07-02 10:23:24','2342dab5-6dc3-46e7-bcaa-737b61c04d36');
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements_sites`
--

DROP TABLE IF EXISTS `elements_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  UNIQUE KEY `elements_sites_uri_siteId_unq_idx` (`uri`,`siteId`),
  KEY `elements_sites_siteId_idx` (`siteId`),
  KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  KEY `elements_sites_enabled_idx` (`enabled`),
  CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements_sites`
--

LOCK TABLES `elements_sites` WRITE;
/*!40000 ALTER TABLE `elements_sites` DISABLE KEYS */;
INSERT INTO `elements_sites` VALUES (1,1,1,NULL,NULL,1,'2018-07-02 06:38:30','2018-07-02 06:38:30','c569f1f9-b8d4-4185-876a-6f585f667ab6'),(2,2,1,'bar','bar/bar',1,'2018-07-02 07:22:15','2018-07-02 07:22:17','e69398a0-1573-42a3-b891-770b9c1ba1b4'),(3,3,1,'restaurant','bar/restaurant',1,'2018-07-02 07:22:24','2018-07-02 07:22:25','753e4def-0bee-4577-979a-8d7acd189bdf'),(4,4,1,'hotel','bar/hotel',1,'2018-07-02 07:22:33','2018-07-02 07:22:34','071d2140-5234-4f6d-8490-c7ec72aaf864'),(5,5,1,'club','bar/club',1,'2018-07-02 07:22:39','2018-07-02 07:22:40','4b534ce0-47f1-4af1-a042-69c94573e053'),(6,6,1,'hello',NULL,1,'2018-07-02 07:24:01','2018-07-02 07:24:01','9e23ec2e-896a-44a6-b160-fb11e48aa4ba'),(7,7,1,'world',NULL,1,'2018-07-02 07:24:05','2018-07-02 07:24:05','f5c7557f-da1e-41a6-9349-743750924d4c'),(8,8,1,'s',NULL,1,'2018-07-02 07:25:06','2018-07-02 07:25:06','559104d6-7095-41c8-9414-444d9e4a2b3a'),(9,9,1,'spirit',NULL,1,'2018-07-02 07:25:14','2018-07-02 07:25:14','f69765b7-01d4-4870-aeec-ad09be48098d'),(10,10,1,'wine',NULL,1,'2018-07-02 07:25:16','2018-07-02 07:25:16','448d24a3-d69d-401e-9a73-16cc3a24090d'),(11,11,1,'champagne',NULL,1,'2018-07-02 07:25:20','2018-07-02 07:25:20','14fa173e-70ff-4881-9b42-ec7ae52eba11'),(12,12,1,'rival-coctail-bar','property-details/rival-coctail-bar',1,'2018-07-02 07:27:12','2018-07-02 10:36:08','5925ea92-af3a-408b-b1a1-8ee78c47a17f'),(13,13,1,NULL,NULL,1,'2018-07-02 07:27:12','2018-07-02 10:36:08','55f20982-2044-4533-b8cc-cc82b812737b'),(14,14,1,NULL,NULL,1,'2018-07-02 09:28:20','2018-07-02 10:12:08','2e80d6ca-71f8-4739-93c3-c4989767ac0f'),(15,15,1,NULL,NULL,1,'2018-07-02 10:12:03','2018-07-02 10:12:03','44b33656-0e5c-4069-86ef-21760cfa8643'),(16,16,1,NULL,NULL,1,'2018-07-02 10:15:24','2018-07-02 10:15:24','7fb86853-c27b-4fb7-9071-463eb3d499ff'),(17,17,1,NULL,NULL,1,'2018-07-02 10:23:23','2018-07-02 10:23:23','201c922f-0d07-4c7b-9680-0ee99bf3dd52'),(18,18,1,NULL,NULL,1,'2018-07-02 10:23:24','2018-07-02 10:23:24','c9fd50a0-13a1-4e6d-a6e6-a015e5da146c');
/*!40000 ALTER TABLE `elements_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_idx` (`authorId`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
INSERT INTO `entries` VALUES (12,1,1,1,'2018-07-02 07:26:00',NULL,'2018-07-02 07:27:12','2018-07-02 10:36:08','6fb3ba27-abba-4fb1-a4be-d7464c6d68e4');
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrydrafts`
--

DROP TABLE IF EXISTS `entrydrafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrydrafts_sectionId_idx` (`sectionId`),
  KEY `entrydrafts_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `entrydrafts_siteId_idx` (`siteId`),
  KEY `entrydrafts_creatorId_idx` (`creatorId`),
  CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrydrafts`
--

LOCK TABLES `entrydrafts` WRITE;
/*!40000 ALTER TABLE `entrydrafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrydrafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrytypes`
--

DROP TABLE IF EXISTS `entrytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_idx` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrytypes`
--

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;
INSERT INTO `entrytypes` VALUES (1,1,4,'Properties','properties',0,NULL,'{propertyName}',1,'2018-07-02 07:17:49','2018-07-02 10:36:07','526667c6-211f-4a6d-9b00-12deba6d628b');
/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entryversions`
--

DROP TABLE IF EXISTS `entryversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `siteId` int(11) NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entryversions_sectionId_idx` (`sectionId`),
  KEY `entryversions_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `entryversions_siteId_idx` (`siteId`),
  KEY `entryversions_creatorId_idx` (`creatorId`),
  CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entryversions`
--

LOCK TABLES `entryversions` WRITE;
/*!40000 ALTER TABLE `entryversions` DISABLE KEYS */;
INSERT INTO `entryversions` VALUES (1,12,1,1,1,1,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Rival Coctail Bar\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 07:27:12','2018-07-02 07:27:12','9ef5b642-1767-4911-8613-289cf8b44c71'),(2,12,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"New\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"14\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 09:30:54','2018-07-02 09:30:54','9bcaafce-715c-4a0f-814f-72270c047137'),(3,12,1,1,1,3,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"New\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"14\",\"15\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 10:12:08','2018-07-02 10:12:08','a05c4207-bdaa-4dc8-88a5-7dad9a7e2eb1'),(4,12,1,1,1,4,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"New\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"17\",\"18\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 10:25:14','2018-07-02 10:25:14','4f537b1c-3d44-402c-89c1-fac1ad9adcb1');
/*!40000 ALTER TABLE `entryversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldgroups`
--

DROP TABLE IF EXISTS `fieldgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldgroups`
--

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;
INSERT INTO `fieldgroups` VALUES (1,'Common','2018-07-02 06:38:30','2018-07-02 06:38:30','51136e91-ae9e-454a-bb10-47ad0602b7db'),(2,'Property Details Page','2018-07-02 07:04:30','2018-07-02 07:13:07','7f51e00a-071d-4e43-8355-ab09a7e978e9');
/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayoutfields`
--

DROP TABLE IF EXISTS `fieldlayoutfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  KEY `fieldlayoutfields_fieldId_idx` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayoutfields`
--

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;
INSERT INTO `fieldlayoutfields` VALUES (51,1,11,8,0,1,'2018-07-02 10:31:07','2018-07-02 10:31:07','e7dc1325-37dc-41b4-8af5-694ff7886b72'),(52,1,11,9,0,2,'2018-07-02 10:31:07','2018-07-02 10:31:07','4e0025bd-297d-43d9-8049-0632d4448f1d'),(80,4,15,1,1,1,'2018-07-02 10:36:07','2018-07-02 10:36:07','0fc91a0e-30ca-44da-b767-a33a3b4fcaf5'),(81,4,15,2,0,2,'2018-07-02 10:36:07','2018-07-02 10:36:07','0a93cc97-a43d-4b28-855b-7bb3c29f5265'),(82,4,15,3,0,3,'2018-07-02 10:36:07','2018-07-02 10:36:07','151b0ac9-42af-4378-aa0f-60da1671b1dd'),(83,4,15,11,0,4,'2018-07-02 10:36:07','2018-07-02 10:36:07','92372fa6-9726-47a2-838e-3dc1041e8fd1'),(84,4,15,6,1,5,'2018-07-02 10:36:07','2018-07-02 10:36:07','4ef1f693-fae7-403f-99e4-70f912d2d832'),(85,4,15,4,0,6,'2018-07-02 10:36:07','2018-07-02 10:36:07','80a7406b-bad1-406f-b862-6db6204ac4d7'),(86,4,15,5,0,7,'2018-07-02 10:36:07','2018-07-02 10:36:07','b5f5dc2c-4073-40fa-b385-22da8cbab727'),(87,4,15,10,0,8,'2018-07-02 10:36:07','2018-07-02 10:36:07','862b1b3d-9674-4fd6-8428-81e75ffcd9d0'),(88,4,15,7,0,9,'2018-07-02 10:36:07','2018-07-02 10:36:07','8116f35a-54df-426c-aecf-878ca2f99e72');
/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouts`
--

DROP TABLE IF EXISTS `fieldlayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouts`
--

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;
INSERT INTO `fieldlayouts` VALUES (1,'craft\\elements\\MatrixBlock','2018-07-02 07:12:06','2018-07-02 10:31:07','fb15d6e3-2165-40aa-8db5-8863e31bacb6'),(2,'craft\\elements\\Category','2018-07-02 07:14:24','2018-07-02 07:22:05','c763cb74-3a3b-4e37-826e-ec92dc904db7'),(3,'craft\\elements\\Tag','2018-07-02 07:15:38','2018-07-02 07:24:27','812afc38-bae2-45bd-8f65-fb10e33d0b9a'),(4,'craft\\elements\\Entry','2018-07-02 07:17:49','2018-07-02 10:36:07','55bb8343-58e2-45e7-8498-198ba7b97d2e'),(5,'craft\\elements\\Asset','2018-07-02 09:27:30','2018-07-02 10:26:05','2dfff8ec-e5fb-4471-ad42-14952c7d13ce');
/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouttabs`
--

DROP TABLE IF EXISTS `fieldlayouttabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_idx` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouttabs`
--

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;
INSERT INTO `fieldlayouttabs` VALUES (11,1,'Content',1,'2018-07-02 10:31:07','2018-07-02 10:31:07','5fc2021f-2507-456d-b23b-058afa0aa3fe'),(15,4,'Property Details',1,'2018-07-02 10:36:07','2018-07-02 10:36:07','7396d1a5-3dc4-4d30-aeb9-ee49598fa741');
/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_groupId_idx` (`groupId`),
  KEY `fields_context_idx` (`context`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES (1,2,'Property Name','propertyName','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:05:39','2018-07-02 07:09:06','223f759e-5dbc-49ea-bd0c-60bd30455489'),(2,2,'Tags','tags','global','','site',NULL,'craft\\fields\\Tags','{\"sources\":\"*\",\"source\":\"taggroup:1\",\"targetSiteId\":null,\"viewMode\":null,\"limit\":null,\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-07-02 07:06:23','2018-07-02 07:23:51','de9dfafb-559a-4f6b-9266-b9781b2be583'),(3,2,'Categories','categories','global','','site',NULL,'craft\\fields\\Categories','{\"branchLimit\":\"\",\"sources\":\"*\",\"source\":\"group:1\",\"targetSiteId\":null,\"viewMode\":null,\"limit\":null,\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-07-02 07:07:49','2018-07-02 07:14:37','6d7b8438-3e3f-4d1f-a304-e22365497714'),(4,2,'Property URL','propertyURL','global','','none',NULL,'craft\\fields\\Url','[]','2018-07-02 07:08:19','2018-07-02 07:09:38','e120e90e-5136-49a7-b9c3-2c5518ab50fc'),(5,2,'Property Email','propertyEmail','global','','none',NULL,'craft\\fields\\Email','[]','2018-07-02 07:08:43','2018-07-02 07:08:43','700e58d3-9f3f-4889-897f-e95600e95431'),(6,2,'Property Number','propertyNumber','global','','none',NULL,'craft\\fields\\Number','{\"defaultValue\":null,\"min\":\"0\",\"max\":null,\"decimals\":0,\"size\":null}','2018-07-02 07:10:04','2018-07-02 07:10:04','87845b7b-490d-4d4d-a146-4c6b67945935'),(7,2,'Coordinates','coordinates','global','','site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"localizeBlocks\":false}','2018-07-02 07:12:05','2018-07-02 10:31:07','e6ca888e-9795-4080-bc3d-f08a1022ad61'),(8,NULL,'Longtitude','longtitude','matrixBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:12:06','2018-07-02 10:31:07','e3158136-e0f3-4915-9858-d4cce039e963'),(9,NULL,'Latitude','latitude','matrixBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:12:06','2018-07-02 10:31:07','e4289f90-e5c2-46c5-af5c-b06222d91f75'),(10,2,'Property Location','propertyLocation','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:12:42','2018-07-02 07:12:42','7a89ab2b-1e48-4258-9e06-9c3e63beb1d5'),(11,2,'Images','images','global','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"1\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"/assets\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-07-02 09:21:39','2018-07-02 10:11:47','3420a3fc-2f64-4f0d-b8e8-0830c7d21c8a');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `globalsets`
--

DROP TABLE IF EXISTS `globalsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `globalsets`
--

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;
/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `edition` tinyint(3) unsigned NOT NULL,
  `timezone` varchar(30) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `on` tinyint(1) NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` VALUES (1,'3.0.13.2','3.0.91',0,'Asia/Tbilisi','Absolut',1,0,'42XlwL0El1YQ','2018-07-02 06:38:30','2018-07-02 10:31:07','9effd330-c2d5-47a7-a96e-c3bc233c1c8f');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocks`
--

DROP TABLE IF EXISTS `matrixblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `matrixblocks_ownerSiteId_idx` (`ownerSiteId`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocks`
--

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;
INSERT INTO `matrixblocks` VALUES (13,12,NULL,7,1,1,'2018-07-02 07:27:12','2018-07-02 10:36:09','ed0c83ab-62fe-4e51-89e6-463d9bf8ae9e');
/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocktypes`
--

DROP TABLE IF EXISTS `matrixblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocktypes`
--

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;
INSERT INTO `matrixblocktypes` VALUES (1,7,1,'CoordinateDetails','coordinateDetails',1,'2018-07-02 07:12:06','2018-07-02 10:31:07','578113b2-3c50-4223-87c7-130c131fc2f5');
/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixcontent_coordinates`
--

DROP TABLE IF EXISTS `matrixcontent_coordinates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_coordinates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_coordinateDetails_longtitude` text,
  `field_coordinateDetails_latitude` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_coordinates_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_coordinates_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_coordinates_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_coordinates_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixcontent_coordinates`
--

LOCK TABLES `matrixcontent_coordinates` WRITE;
/*!40000 ALTER TABLE `matrixcontent_coordinates` DISABLE KEYS */;
INSERT INTO `matrixcontent_coordinates` VALUES (1,13,1,'2018-07-02 07:27:12','2018-07-02 10:36:08','d622d6f4-895b-4c12-9424-d16128b20f6d','14.3423','35.2344');
/*!40000 ALTER TABLE `matrixcontent_coordinates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `migrations_pluginId_idx` (`pluginId`),
  KEY `migrations_type_pluginId_idx` (`type`,`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,NULL,'app','Install','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','bbe31123-0301-4855-9169-d5911014bf1b'),(2,NULL,'app','m150403_183908_migrations_table_changes','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','9059de25-dc4c-4160-bfe6-764d026ff437'),(3,NULL,'app','m150403_184247_plugins_table_changes','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','859cc316-6728-4786-b2cf-ea6a9d7c13e9'),(4,NULL,'app','m150403_184533_field_version','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','167e6f0c-f145-4fa1-87ab-ee8c6d9613f9'),(5,NULL,'app','m150403_184729_type_columns','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','c06e0052-e37d-449c-a44a-954e5869699c'),(6,NULL,'app','m150403_185142_volumes','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','78120c5b-c11f-46ae-86e2-f9e9a5be88ba'),(7,NULL,'app','m150428_231346_userpreferences','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','a46a5b4c-2958-41e5-993d-94d19f119b6e'),(8,NULL,'app','m150519_150900_fieldversion_conversion','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','a0563b96-52ab-4c27-a5e0-64115edf1ca6'),(9,NULL,'app','m150617_213829_update_email_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','3fbd8225-57ef-40b9-a6ed-3111ed6a82e5'),(10,NULL,'app','m150721_124739_templatecachequeries','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f077bed9-1ae7-405b-a78b-3718dc934913'),(11,NULL,'app','m150724_140822_adjust_quality_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','309303f4-4d22-4fd2-ae5c-6486cae3ec61'),(12,NULL,'app','m150815_133521_last_login_attempt_ip','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','0ddadfe9-6e13-4a49-932b-a19721a47446'),(13,NULL,'app','m151002_095935_volume_cache_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','2a96c2b1-759d-452d-881f-d44fab5c1bf1'),(14,NULL,'app','m151005_142750_volume_s3_storage_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','630316ed-c1d5-4e82-b282-3e56197ab2ba'),(15,NULL,'app','m151016_133600_delete_asset_thumbnails','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','96fe5ae2-231b-4e3c-a3ee-e2ed3b26ca7e'),(16,NULL,'app','m151209_000000_move_logo','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f28b144b-969f-4f30-933f-50da757a57cc'),(17,NULL,'app','m151211_000000_rename_fileId_to_assetId','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','5e8420ca-d1d1-43ae-9c6d-6674379d33c9'),(18,NULL,'app','m151215_000000_rename_asset_permissions','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','5c6c796f-4e82-4818-a883-9c9198f952ef'),(19,NULL,'app','m160707_000001_rename_richtext_assetsource_setting','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','a79b409b-bbbd-46b6-90db-ee1dabbad3c1'),(20,NULL,'app','m160708_185142_volume_hasUrls_setting','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','b3204ae5-95d9-4a75-b0d8-eaed2a6ee161'),(21,NULL,'app','m160714_000000_increase_max_asset_filesize','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','9439c41a-d35b-483f-83db-d43a12fa50f9'),(22,NULL,'app','m160727_194637_column_cleanup','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f40d0098-d26e-4795-bc69-47a22217cfdf'),(23,NULL,'app','m160804_110002_userphotos_to_assets','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','3fbc9b77-12c0-470d-9974-277a14e66a56'),(24,NULL,'app','m160807_144858_sites','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','0247155d-01c1-4309-8d98-65b648907054'),(25,NULL,'app','m160829_000000_pending_user_content_cleanup','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','6bc2aa5e-641c-4c60-a593-98e370c4f5df'),(26,NULL,'app','m160830_000000_asset_index_uri_increase','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','32ccabc5-285c-4b3c-99fe-b3a6208e0220'),(27,NULL,'app','m160912_230520_require_entry_type_id','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','e8fedd6e-2698-44ed-b4f7-f1ec31d28f7c'),(28,NULL,'app','m160913_134730_require_matrix_block_type_id','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','858744c8-819b-4c09-b254-c3d28a4af691'),(29,NULL,'app','m160920_174553_matrixblocks_owner_site_id_nullable','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','e4b49820-b718-4b71-8e9b-de5da171a7cd'),(30,NULL,'app','m160920_231045_usergroup_handle_title_unique','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','1d9657f2-41f0-4004-b12f-dfd5e9874e32'),(31,NULL,'app','m160925_113941_route_uri_parts','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f14162bc-fafd-42a9-b3f1-b25def8bf3e6'),(32,NULL,'app','m161006_205918_schemaVersion_not_null','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','2bc31fd6-17ee-4970-90f1-6610c4f4ca41'),(33,NULL,'app','m161007_130653_update_email_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','c9f6044f-58f9-4da0-b184-9cce686b5731'),(34,NULL,'app','m161013_175052_newParentId','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','58293263-c2f4-4248-bf2e-31eb60edc855'),(35,NULL,'app','m161021_102916_fix_recent_entries_widgets','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','d1545ef7-7f15-4fee-a46c-46ea6fcf2a81'),(36,NULL,'app','m161021_182140_rename_get_help_widget','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c918e791-6665-4c9c-8448-485890483c63'),(37,NULL,'app','m161025_000000_fix_char_columns','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','0028dc97-bed9-49e6-87c2-174a15622e6d'),(38,NULL,'app','m161029_124145_email_message_languages','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','13d739fd-dd20-4766-a56d-960e01f91cb7'),(39,NULL,'app','m161108_000000_new_version_format','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','93858e0d-02e9-4105-a18b-81f74f8d8eaa'),(40,NULL,'app','m161109_000000_index_shuffle','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','22a52ec1-7e6f-4618-b87e-aee883c9725a'),(41,NULL,'app','m161122_185500_no_craft_app','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a1c080a8-515c-4eef-b2ac-d6a74c0f8294'),(42,NULL,'app','m161125_150752_clear_urlmanager_cache','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','26b44ee3-f142-4f50-97b8-5db4bfd79606'),(43,NULL,'app','m161220_000000_volumes_hasurl_notnull','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a153d6c6-e90f-4007-bf38-26e1b832e8d9'),(44,NULL,'app','m170114_161144_udates_permission','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2f0398de-50bc-4507-bdfc-55aa89aa09e1'),(45,NULL,'app','m170120_000000_schema_cleanup','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','3aaa5f3b-9b02-4c17-85b1-f0813c7db8f9'),(46,NULL,'app','m170126_000000_assets_focal_point','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c7421fa9-d110-4a47-881e-f4d284932b9f'),(47,NULL,'app','m170206_142126_system_name','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','818dcf8b-8745-46b0-b4d6-486d2f7973e9'),(48,NULL,'app','m170217_044740_category_branch_limits','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','30240d4d-6c8b-4e52-9e94-0614391b4a02'),(49,NULL,'app','m170217_120224_asset_indexing_columns','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','f31ba712-2e96-481a-8257-51b930a28433'),(50,NULL,'app','m170223_224012_plain_text_settings','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','47ded7be-7eb0-4767-b728-f8a36e1227ba'),(51,NULL,'app','m170227_120814_focal_point_percentage','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2095ba2b-cf3a-48c5-a0e0-7b009b4abb1d'),(52,NULL,'app','m170228_171113_system_messages','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c9ed0598-c23d-40f1-8c3c-4cf4a53aae78'),(53,NULL,'app','m170303_140500_asset_field_source_settings','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','0b75f402-6ad2-41de-a6bd-1d541e2e1d4c'),(54,NULL,'app','m170306_150500_asset_temporary_uploads','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','216b7f3f-29ff-4948-86a2-10bb54817a56'),(55,NULL,'app','m170414_162429_rich_text_config_setting','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','05f0e51f-e62e-44cc-a901-f0c11e6d4088'),(56,NULL,'app','m170523_190652_element_field_layout_ids','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','f2a87323-25fe-4d2a-bc9b-2a0341f7b6f2'),(57,NULL,'app','m170612_000000_route_index_shuffle','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','fa1e96ae-ef43-4d6b-9043-0ebf657c16bd'),(58,NULL,'app','m170621_195237_format_plugin_handles','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','845c5151-14ad-4a50-bbce-0946f358cc8f'),(59,NULL,'app','m170630_161028_deprecation_changes','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','64b44c70-6180-480a-ab2f-51616653e584'),(60,NULL,'app','m170703_181539_plugins_table_tweaks','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','df1d0c29-f86c-4054-b8df-b52c8b7260cf'),(61,NULL,'app','m170704_134916_sites_tables','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','f0eb3a19-4d95-429e-b80f-f602e65d9a23'),(62,NULL,'app','m170706_183216_rename_sequences','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2509d865-4238-46bc-8728-62c113bad8b1'),(63,NULL,'app','m170707_094758_delete_compiled_traits','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2d8bfd64-4121-4256-abc4-152b421d1b74'),(64,NULL,'app','m170731_190138_drop_asset_packagist','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','20608848-f911-4381-9873-cdac501a59bf'),(65,NULL,'app','m170810_201318_create_queue_table','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','7ded959a-5f35-4396-993d-213de01d94c1'),(66,NULL,'app','m170816_133741_delete_compiled_behaviors','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c632fd3b-c1b4-49a9-a38e-d9283f3e4afe'),(67,NULL,'app','m170821_180624_deprecation_line_nullable','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','3a07255f-2792-4247-b318-bbffc45bd597'),(68,NULL,'app','m170903_192801_longblob_for_queue_jobs','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','0bd1bfc2-83c0-434d-a6da-7e9e6b08070c'),(69,NULL,'app','m170914_204621_asset_cache_shuffle','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','3f962f1d-2d38-40f5-80b3-762bb3f26ec1'),(70,NULL,'app','m171011_214115_site_groups','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','aeef3a31-98f7-4c9b-a43c-e810dd4587d3'),(71,NULL,'app','m171012_151440_primary_site','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','4d5604f4-221e-486c-b499-0ffa3c48bdda'),(72,NULL,'app','m171013_142500_transform_interlace','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a1ad570e-0253-441f-85ea-556341662de1'),(73,NULL,'app','m171016_092553_drop_position_select','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','522e7151-e574-423b-a678-fc274fd115b1'),(74,NULL,'app','m171016_221244_less_strict_translation_method','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','9609df66-4210-4c5e-96b7-b78109ec6d92'),(75,NULL,'app','m171107_000000_assign_group_permissions','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','dd05853c-a756-486f-81ba-41ddc53b0e81'),(76,NULL,'app','m171117_000001_templatecache_index_tune','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','5f8c0191-b4eb-4704-b8b5-dc298096be29'),(77,NULL,'app','m171126_105927_disabled_plugins','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c75e2d46-f545-4f59-b4b5-5e57a8f393c8'),(78,NULL,'app','m171130_214407_craftidtokens_table','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a8248e18-f7de-4bd1-86bc-1d2c907e7a4c'),(79,NULL,'app','m171202_004225_update_email_settings','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','6dda8fb6-198c-4d94-a3cc-9a0b055b8146'),(80,NULL,'app','m171204_000001_templatecache_index_tune_deux','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','b8e83468-b16e-4213-9923-78702d77e586'),(81,NULL,'app','m171205_130908_remove_craftidtokens_refreshtoken_column','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','388882fa-339c-4fb4-8876-e29e71383b68'),(82,NULL,'app','m171218_143135_longtext_query_column','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','650dd5c2-b420-4c93-a13f-d4c931c3a52c'),(83,NULL,'app','m171231_055546_environment_variables_to_aliases','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','6a6be0e7-1961-4727-9a76-303704db135c'),(84,NULL,'app','m180113_153740_drop_users_archived_column','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','68f9913b-35ec-4ab7-a0a8-d0ce14cdb074'),(85,NULL,'app','m180122_213433_propagate_entries_setting','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','71fa4925-c60a-441a-bbde-6ecac6dd8bb0'),(86,NULL,'app','m180124_230459_fix_propagate_entries_values','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','cd935bb2-311d-455c-8ea2-f98989953972'),(87,NULL,'app','m180128_235202_set_tag_slugs','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','7c10cb78-ca19-41a2-8f7f-30f5bfe791dd'),(88,NULL,'app','m180202_185551_fix_focal_points','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','d9f2a663-f265-4ab7-b86e-ee31c869e8be'),(89,NULL,'app','m180217_172123_tiny_ints','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','fa54358d-baf1-43da-b4fe-2de0105576a6'),(90,NULL,'app','m180321_233505_small_ints','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','cddab551-4633-4c1a-a348-2d2f71ca52e8'),(91,NULL,'app','m180328_115523_new_license_key_statuses','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','619eaec5-fd85-4424-abd8-e0b74020f510'),(92,NULL,'app','m180404_182320_edition_changes','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','737bface-44e1-436f-9a17-0c50b95f03b2'),(93,NULL,'app','m180411_102218_fix_db_routes','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','7955ac79-e2d4-4b09-94c3-b28f15b2ed90'),(94,NULL,'app','m180416_205628_resourcepaths_table','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','24f13ea5-d94f-4333-a539-5c39da2bc0b0'),(95,NULL,'app','m180418_205713_widget_cleanup','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2dec5c04-9964-4e31-82d2-bb3d35e5cfe8');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKey` char(24) DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugins_handle_unq_idx` (`handle`),
  KEY `plugins_enabled_idx` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
INSERT INTO `plugins` VALUES (2,'element-api','2.5.3','1.0.0',NULL,'unknown',1,NULL,'2018-07-02 06:56:30','2018-07-02 06:56:30','2018-07-02 10:16:34','f7dd141e-c85c-4d36-8949-67bd4eac754b');
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  KEY `queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `relations_sourceId_idx` (`sourceId`),
  KEY `relations_targetId_idx` (`targetId`),
  KEY `relations_sourceSiteId_idx` (`sourceSiteId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relations`
--

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
INSERT INTO `relations` VALUES (72,2,12,NULL,11,1,'2018-07-02 10:36:08','2018-07-02 10:36:08','e1e71ecf-1b88-4f72-8f51-9246224a0c1b'),(73,2,12,NULL,9,2,'2018-07-02 10:36:08','2018-07-02 10:36:08','673f3b06-08ed-4ee5-bdea-3cfc883bd994'),(74,2,12,NULL,10,3,'2018-07-02 10:36:08','2018-07-02 10:36:08','5738d037-0392-424a-9a44-d805f78867d5'),(75,3,12,NULL,3,1,'2018-07-02 10:36:08','2018-07-02 10:36:08','26e14d00-396c-4577-8688-733cc073e2fd'),(76,3,12,NULL,4,2,'2018-07-02 10:36:08','2018-07-02 10:36:08','6f9fb60d-768c-402f-b253-92f7ba9d3edf'),(77,3,12,NULL,5,3,'2018-07-02 10:36:08','2018-07-02 10:36:08','65ace8ac-1030-4fb7-876f-c9e19d577eec'),(78,11,12,NULL,17,1,'2018-07-02 10:36:08','2018-07-02 10:36:08','d897b76f-07a2-42f1-a9c8-d90a569b7763'),(79,11,12,NULL,18,2,'2018-07-02 10:36:08','2018-07-02 10:36:08','e725f05a-4c1b-42f0-b51b-2c300410df7a');
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resourcepaths`
--

DROP TABLE IF EXISTS `resourcepaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resourcepaths`
--

LOCK TABLES `resourcepaths` WRITE;
/*!40000 ALTER TABLE `resourcepaths` DISABLE KEYS */;
INSERT INTO `resourcepaths` VALUES ('196f7fc6','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\d3'),('22089940','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\craftsupport\\dist'),('249ff69a','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\xregexp'),('2aa08433','C:\\xampp\\htdocs\\absolut-website\\vendor\\markhuot\\craftql\\src\\resources'),('2d588f0f','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\timepicker'),('2f7d0038','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\jquery-ui'),('2fdc9171','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\searchindexes\\dist'),('32af3fcc','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\element-resize-detector'),('3e296a30','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\recententries\\dist'),('48356173','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\tablesettings\\dist'),('48a23424','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\editcategory\\dist'),('4e5ce663','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\dashboard\\dist'),('4f80f991','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\plugins\\dist'),('545dbf8a','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\fileupload'),('54695466','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\pluginstore\\dist'),('5b9aa87a','@lib'),('63b06982','@bower/jquery/dist'),('68b2f671','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\utilities\\dist'),('6efa2463','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\editentry\\dist'),('7a7cd1dd','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\updater\\dist'),('7e6c9bdb','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\jquery.payment'),('7fafa9df','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\prismjs'),('87185474','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\dbbackup\\dist'),('8b867f43','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\feed\\dist'),('93285e','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\updateswidget\\dist'),('a88f7de3','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\selectize'),('b37650b3','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\generalsettings\\dist'),('b6e914ce','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\sites\\dist'),('b8a489a2','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\installer\\dist'),('c18c7e71','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\fields\\dist'),('c1e967b0','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\garnishjs'),('c8bbbb44','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\picturefill'),('d0de6b84','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\edituser\\dist'),('d4d0f0c2','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\routes\\dist'),('dfccb6bf','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\clearcaches\\dist'),('e378d16b','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\cp\\dist'),('e96fb00b','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\matrix\\dist'),('efc4a524','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\fabric'),('f131cfc6','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\matrixsettings\\dist'),('f5a2aae7','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\velocity'),('ffb747a1','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\jquery-touch-events');
/*!40000 ALTER TABLE `resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) DEFAULT NULL,
  `uriParts` varchar(255) NOT NULL,
  `uriPattern` varchar(255) NOT NULL,
  `template` varchar(500) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `routes_uriPattern_idx` (`uriPattern`),
  KEY `routes_siteId_idx` (`siteId`),
  CONSTRAINT `routes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchindex`
--

DROP TABLE IF EXISTS `searchindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchindex`
--

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;
INSERT INTO `searchindex` VALUES (1,'username',0,1,' goodmorning '),(1,'firstname',0,1,''),(1,'lastname',0,1,''),(1,'fullname',0,1,''),(1,'email',0,1,' sandro goodmorning ge '),(1,'slug',0,1,''),(2,'slug',0,1,' bar '),(2,'title',0,1,' bar '),(3,'slug',0,1,' restaurant '),(3,'title',0,1,' restaurant '),(4,'slug',0,1,' hotel '),(4,'title',0,1,' hotel '),(5,'slug',0,1,' club '),(5,'title',0,1,' club '),(6,'slug',0,1,' hello '),(6,'title',0,1,' hello '),(7,'slug',0,1,' world '),(7,'title',0,1,' world '),(8,'slug',0,1,' s '),(8,'title',0,1,' s '),(9,'slug',0,1,' spirit '),(9,'title',0,1,' spirit '),(10,'slug',0,1,' wine '),(10,'title',0,1,' wine '),(11,'slug',0,1,' champagne '),(11,'title',0,1,' champagne '),(12,'field',1,1,' rival coctail bar '),(12,'field',2,1,' champagne spirit wine '),(12,'field',3,1,' restaurant hotel club '),(12,'field',6,1,' 2147483647 '),(12,'field',4,1,' https website com '),(12,'field',5,1,' email gmail com '),(12,'field',10,1,' gldanula tbilisi georgia '),(12,'field',7,1,' 35 2344 14 3423 '),(13,'field',8,1,' 14 3423 '),(13,'field',9,1,' 35 2344 '),(13,'slug',0,1,''),(12,'slug',0,1,' rival coctail bar '),(12,'title',0,1,' rival coctail bar '),(12,'field',11,1,' screenshot 2 screenshot 3 '),(14,'filename',0,1,' screenshot_3 png '),(14,'extension',0,1,' png '),(14,'kind',0,1,' image '),(14,'slug',0,1,''),(14,'title',0,1,' screenshot 3 '),(15,'filename',0,1,' screenshot_2 png '),(15,'extension',0,1,' png '),(15,'kind',0,1,' image '),(15,'slug',0,1,''),(15,'title',0,1,' screenshot 2 '),(16,'filename',0,1,' screenshot_4 png '),(16,'extension',0,1,' png '),(16,'kind',0,1,' image '),(16,'slug',0,1,''),(16,'title',0,1,' screenshot 4 '),(17,'filename',0,1,' screenshot_2_180702_102323 png '),(17,'extension',0,1,' png '),(17,'kind',0,1,' image '),(17,'slug',0,1,''),(17,'title',0,1,' screenshot 2 '),(18,'filename',0,1,' screenshot_3_180702_102323 png '),(18,'extension',0,1,' png '),(18,'kind',0,1,' image '),(18,'slug',0,1,''),(18,'title',0,1,' screenshot 3 ');
/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagateEntries` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_handle_unq_idx` (`handle`),
  UNIQUE KEY `sections_name_unq_idx` (`name`),
  KEY `sections_structureId_idx` (`structureId`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,NULL,'Properties','properties','channel',1,1,'2018-07-02 07:17:49','2018-07-02 07:18:17','5d374a8d-c95c-4fc0-94e4-5478d810295a');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections_sites`
--

DROP TABLE IF EXISTS `sections_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  KEY `sections_sites_siteId_idx` (`siteId`),
  CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections_sites`
--

LOCK TABLES `sections_sites` WRITE;
/*!40000 ALTER TABLE `sections_sites` DISABLE KEYS */;
INSERT INTO `sections_sites` VALUES (1,1,1,1,'property-details/{slug}','property-details/_entry',1,'2018-07-02 07:17:49','2018-07-02 07:18:17','9daf9764-0e9a-488f-a3da-6cec41e2f2e1');
/*!40000 ALTER TABLE `sections_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_idx` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shunnedmessages`
--

DROP TABLE IF EXISTS `shunnedmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shunnedmessages`
--

LOCK TABLES `shunnedmessages` WRITE;
/*!40000 ALTER TABLE `shunnedmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `shunnedmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitegroups`
--

DROP TABLE IF EXISTS `sitegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sitegroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitegroups`
--

LOCK TABLES `sitegroups` WRITE;
/*!40000 ALTER TABLE `sitegroups` DISABLE KEYS */;
INSERT INTO `sitegroups` VALUES (1,'Absolut','2018-07-02 06:38:30','2018-07-02 06:38:30','e0948488-a3b2-4277-a33d-6e1e15bd81c7');
/*!40000 ALTER TABLE `sitegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sites_handle_unq_idx` (`handle`),
  KEY `sites_sortOrder_idx` (`sortOrder`),
  KEY `sites_groupId_fk` (`groupId`),
  CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,1,1,'Absolut','default','en-US',1,'@web/',1,'2018-07-02 06:38:30','2018-07-02 06:38:30','13ed0bda-8756-4389-8cab-7be777651453');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structureelements`
--

DROP TABLE IF EXISTS `structureelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_idx` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structureelements`
--

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;
INSERT INTO `structureelements` VALUES (1,1,NULL,1,1,10,0,'2018-07-02 07:22:16','2018-07-02 07:22:39','748ee4ad-2673-426b-a653-846f16b1f885'),(2,1,2,1,2,3,1,'2018-07-02 07:22:16','2018-07-02 07:22:16','733f77c3-76f5-4db5-8263-b8b537567a83'),(3,1,3,1,4,5,1,'2018-07-02 07:22:24','2018-07-02 07:22:24','67e6e21b-4853-4ec6-a595-9745af6aa43c'),(4,1,4,1,6,7,1,'2018-07-02 07:22:33','2018-07-02 07:22:33','60cc8a4a-4d8a-46a0-a7f5-f034c0a6092c'),(5,1,5,1,8,9,1,'2018-07-02 07:22:39','2018-07-02 07:22:39','1df64379-f03c-44c5-aa8a-156438eef721');
/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
INSERT INTO `structures` VALUES (1,NULL,'2018-07-02 07:14:24','2018-07-02 07:22:05','4a22fc7f-a1f1-411f-ba7b-5da15575a2a4');
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemmessages`
--

DROP TABLE IF EXISTS `systemmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  KEY `systemmessages_language_idx` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemmessages`
--

LOCK TABLES `systemmessages` WRITE;
/*!40000 ALTER TABLE `systemmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemsettings`
--

DROP TABLE IF EXISTS `systemsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemsettings`
--

LOCK TABLES `systemsettings` WRITE;
/*!40000 ALTER TABLE `systemsettings` DISABLE KEYS */;
INSERT INTO `systemsettings` VALUES (1,'email','{\"fromEmail\":\"sandro@goodmorning.ge\",\"fromName\":\"Absolut\",\"transportType\":\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"}','2018-07-02 06:38:31','2018-07-02 06:38:31','551dab7c-4b1b-4abe-869e-a8039d2f27c9');
/*!40000 ALTER TABLE `systemsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggroups`
--

DROP TABLE IF EXISTS `taggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `taggroups_handle_unq_idx` (`handle`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggroups`
--

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;
INSERT INTO `taggroups` VALUES (1,'tags','tags',3,'2018-07-02 07:15:38','2018-07-02 07:24:27','46a42c5f-d3f1-4449-bb40-7cd3f3c3a655');
/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_idx` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (6,1,'2018-07-02 07:24:01','2018-07-02 07:24:01','447e889d-062b-4c48-b987-2138e32ac47d'),(7,1,'2018-07-02 07:24:05','2018-07-02 07:24:05','67819d2b-e2a5-4a7b-a8d3-866923463b0c'),(8,1,'2018-07-02 07:25:06','2018-07-02 07:25:06','8920fbb2-1ed9-476f-a796-9fa3f993fda9'),(9,1,'2018-07-02 07:25:14','2018-07-02 07:25:14','4af4d77c-5a2a-46d9-8f8f-ea9a8829ac5a'),(10,1,'2018-07-02 07:25:16','2018-07-02 07:25:16','c466bf86-f6cd-4fed-a54c-94972bbcb295'),(11,1,'2018-07-02 07:25:20','2018-07-02 07:25:20','495adbce-8ee0-4e6d-9628-09bf7ade1841');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecacheelements`
--

DROP TABLE IF EXISTS `templatecacheelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  KEY `templatecacheelements_elementId_idx` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecacheelements`
--

LOCK TABLES `templatecacheelements` WRITE;
/*!40000 ALTER TABLE `templatecacheelements` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecacheelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecachequeries`
--

DROP TABLE IF EXISTS `templatecachequeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  KEY `templatecachequeries_type_idx` (`type`),
  CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecachequeries`
--

LOCK TABLES `templatecachequeries` WRITE;
/*!40000 ALTER TABLE `templatecachequeries` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecachequeries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecaches`
--

DROP TABLE IF EXISTS `templatecaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `templatecaches_siteId_idx` (`siteId`),
  CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecaches`
--

LOCK TABLES `templatecaches` WRITE;
/*!40000 ALTER TABLE `templatecaches` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  UNIQUE KEY `usergroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups_users`
--

DROP TABLE IF EXISTS `usergroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_idx` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups_users`
--

LOCK TABLES `usergroups_users` WRITE;
/*!40000 ALTER TABLE `usergroups_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions`
--

DROP TABLE IF EXISTS `userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions`
--

LOCK TABLES `userpermissions` WRITE;
/*!40000 ALTER TABLE `userpermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_usergroups`
--

DROP TABLE IF EXISTS `userpermissions_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_idx` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_usergroups`
--

LOCK TABLES `userpermissions_usergroups` WRITE;
/*!40000 ALTER TABLE `userpermissions_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_users`
--

DROP TABLE IF EXISTS `userpermissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_idx` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_users`
--

LOCK TABLES `userpermissions_users` WRITE;
/*!40000 ALTER TABLE `userpermissions_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpreferences`
--

DROP TABLE IF EXISTS `userpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text,
  PRIMARY KEY (`userId`),
  CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpreferences`
--

LOCK TABLES `userpreferences` WRITE;
/*!40000 ALTER TABLE `userpreferences` DISABLE KEYS */;
INSERT INTO `userpreferences` VALUES (1,'{\"language\":\"en-US\"}');
/*!40000 ALTER TABLE `userpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unq_idx` (`username`),
  UNIQUE KEY `users_email_unq_idx` (`email`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_photoId_fk` (`photoId`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'goodmorning',NULL,NULL,NULL,'sandro@goodmorning.ge','$2y$13$zRQCuXEG0UkNeSAlz5Z71uv1/2hx.Yhew8nh.NH19BtEkVsjMU4Bi',1,0,0,0,'2018-07-02 06:38:31','::1',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:34','46b37ac9-30ad-47af-9635-6d89ded0eb80');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumefolders`
--

DROP TABLE IF EXISTS `volumefolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  KEY `volumefolders_parentId_idx` (`parentId`),
  KEY `volumefolders_volumeId_idx` (`volumeId`),
  CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumefolders`
--

LOCK TABLES `volumefolders` WRITE;
/*!40000 ALTER TABLE `volumefolders` DISABLE KEYS */;
INSERT INTO `volumefolders` VALUES (1,NULL,1,'Property Images','','2018-07-02 09:27:30','2018-07-02 09:27:30','5be3e40a-49ef-4986-8e00-be14cfd449ca'),(2,NULL,NULL,'Temporary source',NULL,'2018-07-02 09:28:11','2018-07-02 09:28:11','4001550c-0f44-488f-992d-ef2ca1b6b289'),(3,2,NULL,'user_1','user_1/','2018-07-02 09:28:11','2018-07-02 09:28:11','f5d97488-2d6f-4f37-8c3d-ad58847bdcbb'),(4,1,1,'rival-coctail-bar','rival-coctail-bar/','2018-07-02 09:30:53','2018-07-02 09:30:53','ccd24f1a-ab63-44a1-9897-55586c7dffbe');
/*!40000 ALTER TABLE `volumefolders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumes_name_unq_idx` (`name`),
  UNIQUE KEY `volumes_handle_unq_idx` (`handle`),
  KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
INSERT INTO `volumes` VALUES (1,5,'Property Images','propertyImages','craft\\volumes\\Local',1,'http://localhost/absolut-website/web/assets','{\"path\":\"assets\"}',1,'2018-07-02 09:27:30','2018-07-02 10:26:05','f7d2f635-0de5-4dcf-8219-a5a14ff12946');
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_idx` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,1,'craft\\widgets\\RecentEntries',1,0,'{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','cc104fc4-a4d5-4bb6-b087-b1fb1c524cb9'),(2,1,'craft\\widgets\\CraftSupport',2,0,'[]',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','8496541a-8695-400b-95d4-72acdd170875'),(3,1,'craft\\widgets\\Updates',3,0,'[]',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','f9d70378-cb2c-4b9a-abec-647b69ef8942'),(4,1,'craft\\widgets\\Feed',4,0,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','576ebca0-58e3-4897-9d3b-562487d25daa');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-02 14:58:30
