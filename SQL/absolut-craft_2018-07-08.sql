-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: test
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.33-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assetindexdata`
--

DROP TABLE IF EXISTS `assetindexdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  KEY `assetindexdata_volumeId_idx` (`volumeId`),
  CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assetindexdata`
--

LOCK TABLES `assetindexdata` WRITE;
/*!40000 ALTER TABLE `assetindexdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `assetindexdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assets_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `assets_folderId_idx` (`folderId`),
  KEY `assets_volumeId_idx` (`volumeId`),
  CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (14,1,1,'Screenshot_3.png','image',1022,1093,72298,NULL,'2018-07-02 09:30:53','2018-07-02 09:28:22','2018-07-02 10:12:08','e3cba627-5457-4452-b291-f2d668d750f3'),(15,1,1,'Screenshot_2.png','image',571,296,21814,NULL,'2018-07-02 10:12:03','2018-07-02 10:12:03','2018-07-02 10:12:03','80e18d51-2082-4451-bd5a-a839ba21a8f8'),(16,1,1,'Screenshot_4.png','image',93,522,19376,NULL,'2018-07-02 10:15:24','2018-07-02 10:15:24','2018-07-02 10:15:24','59e8ddf8-0793-4e83-884e-e439bef1735b'),(17,1,1,'Screenshot_2_180702_102323.png','image',571,296,21814,NULL,'2018-07-02 10:23:23','2018-07-02 10:23:23','2018-07-02 10:23:23','425f6a78-4f25-47b4-8d99-49bbd572324f'),(18,1,1,'Screenshot_3_180702_102323.png','image',1022,1093,72298,NULL,'2018-07-02 10:23:25','2018-07-02 10:23:25','2018-07-02 10:23:25','40dcd00b-7965-4d5d-8b15-9d7639342506');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransformindex`
--

DROP TABLE IF EXISTS `assettransformindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransformindex`
--

LOCK TABLES `assettransformindex` WRITE;
/*!40000 ALTER TABLE `assettransformindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransforms`
--

DROP TABLE IF EXISTS `assettransforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransforms`
--

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;
/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_idx` (`groupId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (2,1,'2018-07-02 07:22:15','2018-07-02 07:22:15','43388d9a-70e7-475d-b77b-6ed8e755337b'),(3,1,'2018-07-02 07:22:24','2018-07-02 07:22:24','fb2c3fbd-75dc-4ba3-b6d5-429ab64c1e2f'),(4,1,'2018-07-02 07:22:33','2018-07-02 07:22:33','ae777979-c5ca-4715-8cc1-b6f04aa50bc7'),(5,1,'2018-07-02 07:22:39','2018-07-02 07:22:39','06ffac7f-b97a-493c-98a0-60cbdce34ae7');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups`
--

DROP TABLE IF EXISTS `categorygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `categorygroups_handle_unq_idx` (`handle`),
  KEY `categorygroups_structureId_idx` (`structureId`),
  KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups`
--

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;
INSERT INTO `categorygroups` VALUES (1,1,2,'Property Types','propertyTypes','2018-07-02 07:14:24','2018-07-02 07:22:05','e331ad79-e7e5-401a-bdf0-bedb1ebf2b45');
/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups_sites`
--

DROP TABLE IF EXISTS `categorygroups_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  KEY `categorygroups_sites_siteId_idx` (`siteId`),
  CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups_sites`
--

LOCK TABLES `categorygroups_sites` WRITE;
/*!40000 ALTER TABLE `categorygroups_sites` DISABLE KEYS */;
INSERT INTO `categorygroups_sites` VALUES (1,1,1,1,'bar/{slug}','bar/_category','2018-07-02 07:14:24','2018-07-02 07:22:05','b0a108f4-9e71-4cb2-9822-2f4c7b6f5bd2');
/*!40000 ALTER TABLE `categorygroups_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_propertyName` text,
  `field_propertyURL` text,
  `field_propertyEmail` varchar(255) DEFAULT NULL,
  `field_propertyNumber` int(10) DEFAULT NULL,
  `field_propertyLocation` text,
  `field_city` text,
  `field_district` text,
  `field_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `content_siteId_idx` (`siteId`),
  KEY `content_title_idx` (`title`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,1,1,NULL,'2018-07-02 06:38:30','2018-07-02 06:38:30','d0460f49-37c9-4ab3-a790-7ed38e7a1d7f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,1,'Bar','2018-07-02 07:22:15','2018-07-02 07:22:15','3b6fa810-2a30-49f8-8333-6e28ec6c7d97',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,3,1,'Restaurant','2018-07-02 07:22:24','2018-07-02 07:22:24','4cf03788-0eb1-4fc5-b366-15cd5c9db152',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,4,1,'Hotel','2018-07-02 07:22:33','2018-07-02 07:22:33','6e6338cb-9c4d-49e2-8565-6bcef1771880',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,5,1,'Club','2018-07-02 07:22:39','2018-07-02 07:22:39','d0656b7f-b0b6-47ca-911d-077f094d19f1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,6,1,'hello','2018-07-02 07:24:01','2018-07-02 07:24:01','269e8930-020c-44e5-9f68-a290582ab65c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,7,1,'world','2018-07-02 07:24:05','2018-07-02 07:24:05','6b5efb27-f354-40b5-9797-6804c9319fe3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,8,1,'S','2018-07-02 07:25:06','2018-07-02 07:25:06','4a84a749-7d0d-4ca1-9c28-5b5b38f6b15e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,9,1,'Spirit','2018-07-02 07:25:14','2018-07-02 07:25:14','128fcaea-5ea4-4389-8158-7e5348d832f0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,10,1,'Wine','2018-07-02 07:25:16','2018-07-02 07:25:16','2d8b9c77-6f48-440b-aa8f-2801586dfcfe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,11,1,'Champagne','2018-07-02 07:25:20','2018-07-02 07:25:20','96cb4e08-6008-4bab-be69-4364d4608a65',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,12,1,'Rival Coctail Bar','2018-07-02 07:27:12','2018-07-06 07:29:45','48ef8cbf-558c-4725-8d6b-fecb73fb0768','Rival Coctail Bar','https://website.com','email@gmail.com',2147483647,'Gldanula, Tbilisi, Georgia',NULL,NULL,NULL),(13,14,1,'Screenshot 3','2018-07-02 09:28:20','2018-07-02 10:12:08','80cfb66b-130e-42f9-9ec9-4a809fcb9cc4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,15,1,'Screenshot 2','2018-07-02 10:12:03','2018-07-02 10:12:03','1c4fb053-2ca2-4715-9f75-95c9d70ef818',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,16,1,'Screenshot 4','2018-07-02 10:15:24','2018-07-02 10:15:24','ded6b22d-d562-4f4a-8d87-a1f4fe615a4b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,17,1,'Screenshot 2','2018-07-02 10:23:23','2018-07-02 10:23:23','d9ede8ae-2219-4117-bec5-6ae2c60e8e8c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,18,1,'Screenshot 3','2018-07-02 10:23:24','2018-07-02 10:23:24','0b0b0e2c-a7b5-4b88-8094-e6dc3e9d2efd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,19,1,'Coca Cola Burger','2018-07-02 12:55:31','2018-07-06 07:29:45','af629148-d011-43ef-9d13-ee9c64bd33a3','Coca Cola Burger','','',321432,NULL,NULL,NULL,NULL),(19,20,1,'burger','2018-07-04 09:19:58','2018-07-04 09:19:58','0fed520e-e52d-4539-b879-23eacc17d16d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,21,1,'bar','2018-07-04 09:20:01','2018-07-04 09:20:01','3688b688-ad24-433b-9340-84bdc09d6bf8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,22,1,'cocktails','2018-07-04 09:20:11','2018-07-04 09:20:11','0e613548-344a-46c0-9567-641081bbb457',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,23,1,'Burger Bar','2018-07-04 09:21:21','2018-07-06 07:29:45','655ce9c5-53d9-4aa4-8103-6d1a58918b36','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(23,25,1,'Hello','2018-07-04 09:29:41','2018-07-06 07:30:03','5849cb82-eba5-477a-8950-d37f3af440af','Hello','','',342,NULL,NULL,NULL,'fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal'),(24,26,1,'Hello','2018-07-06 11:06:19','2018-07-06 11:06:19','d5e7addb-b5fe-4c78-a67e-de41ecff98e9','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(25,27,1,'Hello','2018-07-06 11:06:30','2018-07-06 11:06:30','07860123-4ece-4a54-a44e-3163d241aa79','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(26,28,1,'Hello','2018-07-06 11:06:47','2018-07-06 11:06:47','b162ccd3-cd87-4e48-9d98-3d31387247a0','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(27,29,1,'Hello','2018-07-06 11:06:49','2018-07-06 11:06:50','84fa3441-09e3-4e96-8ea6-0b9a5630ac61','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(28,30,1,'Hello','2018-07-06 11:06:52','2018-07-06 11:06:52','63cf9807-52a0-4036-bb66-248dd86873a5','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(29,31,1,'Hello','2018-07-06 11:06:54','2018-07-06 11:06:54','f9e14981-df42-43c4-8e84-e4ad7a25819a','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(30,32,1,'Hello','2018-07-06 11:06:57','2018-07-06 11:06:57','4ac94532-b028-4be6-b9bf-eeb331f52716','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(31,33,1,'Hello','2018-07-06 11:07:00','2018-07-06 11:07:00','096dee31-b40a-4b5a-8e28-393d827c77ce','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(32,34,1,'Hello','2018-07-06 11:07:14','2018-07-06 11:07:15','5513c68a-bfdb-453f-a5b0-0fd21cd95923','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(33,35,1,'Hello','2018-07-06 11:07:17','2018-07-06 11:07:17','c783d9d3-6822-4c52-b31f-1f8005bc5f56','Hello',NULL,'',342,NULL,NULL,NULL,'<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>'),(34,36,1,'Burger Bar','2018-07-06 11:07:29','2018-07-06 11:07:29','6439f600-6792-4280-b1c8-259a09a3aeb8','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(35,38,1,'Burger Bar','2018-07-06 11:07:32','2018-07-06 11:07:32','b6d167fe-2c7c-47f6-bf4a-6415e46a39b2','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(36,40,1,'Burger Bar','2018-07-06 11:07:35','2018-07-06 11:07:35','ab3ff13d-111d-4c73-abb9-7bd5ddc7243a','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(37,42,1,'Burger Bar','2018-07-06 11:07:39','2018-07-06 11:07:39','c32832de-b336-475a-b66c-896e36eb2ac3','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(38,44,1,'Burger Bar','2018-07-06 11:07:42','2018-07-06 11:07:42','91b820e7-4046-4736-85ba-6aa7100dfffd','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(39,46,1,'Burger Bar','2018-07-06 11:07:47','2018-07-06 11:07:47','4b1eed30-5387-4473-8da7-0883a3db1663','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL),(40,48,1,'Burger Bar','2018-07-06 11:07:51','2018-07-06 11:07:51','7d58ba97-ea51-4bb9-9b37-9641af73f835','Burger Bar','http://site.com','mail@hotmail.com',78328932,'Georgia','Tbilisi','Saburtalo',NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `craftidtokens`
--

DROP TABLE IF EXISTS `craftidtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craftidtokens_userId_fk` (`userId`),
  CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `craftidtokens`
--

LOCK TABLES `craftidtokens` WRITE;
/*!40000 ALTER TABLE `craftidtokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `craftidtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deprecationerrors`
--

DROP TABLE IF EXISTS `deprecationerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deprecationerrors`
--

LOCK TABLES `deprecationerrors` WRITE;
/*!40000 ALTER TABLE `deprecationerrors` DISABLE KEYS */;
INSERT INTO `deprecationerrors` VALUES (1,'ElementQuery::getIterator()','C:\\xampp\\htdocs\\craft\\config\\element-api.php:54','2018-07-03 09:25:07','C:\\xampp\\htdocs\\craft\\config\\element-api.php',54,'Looping through element queries directly has been deprecated. Use the all() function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":410,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\CategoryQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\config\\\\element-api.php\",\"line\":54,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":null,\"file\":null,\"line\":null,\"class\":\"craft\\\\services\\\\Config\",\"method\":\"{closure}\",\"args\":\"craft\\\\elements\\\\Entry\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\league\\\\fractal\\\\src\\\\Scope.php\",\"line\":373,\"class\":null,\"method\":\"call_user_func\",\"args\":\"Closure, craft\\\\elements\\\\Entry\"},{\"objectClass\":\"League\\\\Fractal\\\\Scope\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\league\\\\fractal\\\\src\\\\Scope.php\",\"line\":318,\"class\":\"League\\\\Fractal\\\\Scope\",\"method\":\"fireTransformer\",\"args\":\"Closure, craft\\\\elements\\\\Entry\"},{\"objectClass\":\"League\\\\Fractal\\\\Scope\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\league\\\\fractal\\\\src\\\\Scope.php\",\"line\":234,\"class\":\"League\\\\Fractal\\\\Scope\",\"method\":\"executeResourceTransformers\",\"args\":null},{\"objectClass\":\"League\\\\Fractal\\\\Scope\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\craftcms\\\\element-api\\\\src\\\\controllers\\\\DefaultController.php\",\"line\":155,\"class\":\"League\\\\Fractal\\\\Scope\",\"method\":\"toArray\",\"args\":null},{\"objectClass\":\"craft\\\\elementapi\\\\controllers\\\\DefaultController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\elementapi\\\\controllers\\\\DefaultController\",\"method\":\"actionIndex\",\"args\":\"\\\"property/<titleSearch:\\\\d+>.json\\\"\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\elementapi\\\\controllers\\\\DefaultController, \\\"actionIndex\\\"], [\\\"property/<titleSearch:\\\\d+>.json\\\"]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"pattern\\\" => \\\"property/<titleSearch:\\\\d+>.json\\\", \\\"titleSearch\\\" => \\\"12\\\", \\\"p\\\" => \\\"property/12.json\\\"]\"},{\"objectClass\":\"craft\\\\elementapi\\\\controllers\\\\DefaultController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":103,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"\\\", [\\\"pattern\\\" => \\\"property/<titleSearch:\\\\d+>.json\\\", \\\"titleSearch\\\" => \\\"12\\\", \\\"p\\\" => \\\"property/12.json\\\"]\"},{\"objectClass\":\"craft\\\\elementapi\\\\controllers\\\\DefaultController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"\\\", [\\\"pattern\\\" => \\\"property/<titleSearch:\\\\d+>.json\\\", \\\"titleSearch\\\" => \\\"12\\\", \\\"p\\\" => \\\"property/12.json\\\"]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":282,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"element-api\\\", [\\\"pattern\\\" => \\\"property/<titleSearch:\\\\d+>.json\\\", \\\"titleSearch\\\" => \\\"12\\\", \\\"p\\\" => \\\"property/12.json\\\"]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"element-api\\\", [\\\"pattern\\\" => \\\"property/<titleSearch:\\\\d+>.json\\\", \\\"titleSearch\\\" => \\\"12\\\", \\\"p\\\" => \\\"property/12.json\\\"]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":271,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2018-07-03 09:25:07','2018-07-03 09:25:07','6c3c55cb-efd8-44d1-832f-54e03ae291ce');
/*!40000 ALTER TABLE `deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementindexsettings`
--

DROP TABLE IF EXISTS `elementindexsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementindexsettings`
--

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` VALUES (1,NULL,'craft\\elements\\User',1,0,'2018-07-02 06:38:30','2018-07-02 06:38:30','ec036719-ecad-46eb-a770-e3ab5ec793e9'),(2,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:15','2018-07-02 07:22:15','86259402-cacf-4769-83a8-9993bb5f8542'),(3,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:24','2018-07-02 07:22:24','00815aa0-abc2-4ba8-9cb9-1bec767732bb'),(4,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:33','2018-07-02 07:22:33','4b2322bd-8106-44cb-b7e5-1194f2046458'),(5,2,'craft\\elements\\Category',1,0,'2018-07-02 07:22:39','2018-07-02 07:22:39','2414f776-c76c-4baf-97b6-ebe5db58adc5'),(6,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:24:01','2018-07-02 07:24:01','35e75cb0-ad97-475d-90cc-8fcc244a270e'),(7,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:24:05','2018-07-02 07:24:05','35a2a7c5-12d5-4e46-ae5f-7e1e3a178670'),(8,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:06','2018-07-02 07:25:06','638ba1a3-674c-487a-a79f-d38600c8cfaf'),(9,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:14','2018-07-02 07:25:14','c2fc31cc-ee02-4483-a67c-c4e92fd1ee92'),(10,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:16','2018-07-02 07:25:16','508c607a-5f35-4091-a7df-9d849f8d73b2'),(11,3,'craft\\elements\\Tag',1,0,'2018-07-02 07:25:20','2018-07-02 07:25:20','24c3c087-c2f2-47f2-b495-c630c3ea9e5d'),(12,4,'craft\\elements\\Entry',1,0,'2018-07-02 07:27:12','2018-07-06 07:29:45','2dd4d5e2-8086-4715-92fa-3a1ef5dcb8df'),(13,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-02 07:27:12','2018-07-06 07:29:45','37e7ad5f-e656-4f42-9491-ddca2d2059b6'),(14,5,'craft\\elements\\Asset',1,0,'2018-07-02 09:28:20','2018-07-02 10:12:08','afa1f552-d1d2-4c31-bae3-367b550a3005'),(15,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:12:03','2018-07-02 10:12:03','7a1685d0-35a8-4306-9d20-c1b16299928d'),(16,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:15:24','2018-07-02 10:15:24','3d97166f-7fe7-4f9e-9d5b-17f12a6aef73'),(17,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:23:23','2018-07-02 10:23:23','a6f5f3c1-647f-4ee8-8f48-aa1e887f92ad'),(18,5,'craft\\elements\\Asset',1,0,'2018-07-02 10:23:24','2018-07-02 10:23:24','2342dab5-6dc3-46e7-bcaa-737b61c04d36'),(19,4,'craft\\elements\\Entry',1,0,'2018-07-02 12:55:31','2018-07-06 07:29:45','1435308e-4b05-401b-9895-c484691506ed'),(20,3,'craft\\elements\\Tag',1,0,'2018-07-04 09:19:58','2018-07-04 09:19:58','58da4a67-adb0-4f1c-94db-6e1fe745e1a5'),(21,3,'craft\\elements\\Tag',1,0,'2018-07-04 09:20:01','2018-07-04 09:20:01','0cff0142-6ce7-41f3-97f9-12b4a22ab860'),(22,3,'craft\\elements\\Tag',1,0,'2018-07-04 09:20:11','2018-07-04 09:20:11','6fbd7b71-4253-4d9f-b3a0-713a7e698881'),(23,4,'craft\\elements\\Entry',1,0,'2018-07-04 09:21:21','2018-07-06 07:29:45','fa6a075e-5131-4c88-b8e7-d6681155aa90'),(24,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-04 09:21:21','2018-07-06 07:29:46','f4bc46f2-535e-4c95-9f46-b9898c52f26b'),(25,4,'craft\\elements\\Entry',1,0,'2018-07-04 09:29:41','2018-07-06 07:30:03','bdb73c22-863d-421a-a275-44877c398ba7'),(26,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:19','2018-07-06 11:06:19','932782c0-cd72-4bf4-b407-710a6bd64729'),(27,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:30','2018-07-06 11:06:30','3ca4e673-b9b2-4d9f-8bdd-2ee584f1f07f'),(28,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:47','2018-07-06 11:06:47','ebbc4384-67fb-4239-8d7b-21a21683a30b'),(29,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:49','2018-07-06 11:06:50','87780164-35fa-4cad-bc5d-89d66eab8049'),(30,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:52','2018-07-06 11:06:52','d5827a02-aada-4891-9ece-99b90233778e'),(31,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:54','2018-07-06 11:06:54','6892c1a8-dc53-4b4b-ad07-d0237e496410'),(32,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:06:57','2018-07-06 11:06:57','687f2f30-b102-4a65-8c29-c93a6ca90ef1'),(33,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:00','2018-07-06 11:07:00','dfeb6dc1-46b4-42c9-a29a-f7e2ac2d862e'),(34,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:14','2018-07-06 11:07:15','eae538f2-3645-4ecc-b042-6137a54c866d'),(35,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:17','2018-07-06 11:07:17','056fb2bf-ee86-4a81-82fc-9fedda031403'),(36,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:29','2018-07-06 11:07:29','57c6166d-2950-4eaa-8087-732584584f45'),(37,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:29','2018-07-06 11:07:29','732826b4-fff4-4e2c-98d0-61c10d6ff78c'),(38,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:32','2018-07-06 11:07:32','4ef34ab7-60e2-4248-9e92-41af9885dd9e'),(39,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:32','2018-07-06 11:07:32','9ca9d04b-5deb-455d-9a3b-e9da73070747'),(40,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:35','2018-07-06 11:07:35','7d461a78-6451-41cb-b709-0a3704240ffa'),(41,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:35','2018-07-06 11:07:35','19522fbd-5e03-462c-a05f-48227fa49dcc'),(42,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:39','2018-07-06 11:07:39','d213f8f8-bd40-4a8f-b2ca-c71661adaede'),(43,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:39','2018-07-06 11:07:39','976d6189-198b-40f9-9817-de57a5ff2d60'),(44,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:42','2018-07-06 11:07:42','3214434e-864f-466f-871f-33e1d2adda81'),(45,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:42','2018-07-06 11:07:42','9eb93547-f4e0-4d88-83bd-9348b4628640'),(46,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:47','2018-07-06 11:07:47','82783dab-e26d-4d61-8e1a-cb758dda3166'),(47,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:47','2018-07-06 11:07:47','3e441ec8-e067-4d64-8e16-6a98a9c6c1ad'),(48,4,'craft\\elements\\Entry',1,0,'2018-07-06 11:07:51','2018-07-06 11:07:51','fa4659f2-6822-4e7d-ab57-bb6f18550738'),(49,1,'craft\\elements\\MatrixBlock',1,0,'2018-07-06 11:07:51','2018-07-06 11:07:51','4078c189-ceb8-419e-8682-19f8e33844b6');
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements_sites`
--

DROP TABLE IF EXISTS `elements_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  UNIQUE KEY `elements_sites_uri_siteId_unq_idx` (`uri`,`siteId`),
  KEY `elements_sites_siteId_idx` (`siteId`),
  KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  KEY `elements_sites_enabled_idx` (`enabled`),
  CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements_sites`
--

LOCK TABLES `elements_sites` WRITE;
/*!40000 ALTER TABLE `elements_sites` DISABLE KEYS */;
INSERT INTO `elements_sites` VALUES (1,1,1,NULL,NULL,1,'2018-07-02 06:38:30','2018-07-02 06:38:30','c569f1f9-b8d4-4185-876a-6f585f667ab6'),(2,2,1,'bar','bar/bar',1,'2018-07-02 07:22:15','2018-07-02 07:22:17','e69398a0-1573-42a3-b891-770b9c1ba1b4'),(3,3,1,'restaurant','bar/restaurant',1,'2018-07-02 07:22:24','2018-07-02 07:22:25','753e4def-0bee-4577-979a-8d7acd189bdf'),(4,4,1,'hotel','bar/hotel',1,'2018-07-02 07:22:33','2018-07-02 07:22:34','071d2140-5234-4f6d-8490-c7ec72aaf864'),(5,5,1,'club','bar/club',1,'2018-07-02 07:22:39','2018-07-02 07:22:40','4b534ce0-47f1-4af1-a042-69c94573e053'),(6,6,1,'hello',NULL,1,'2018-07-02 07:24:01','2018-07-02 07:24:01','9e23ec2e-896a-44a6-b160-fb11e48aa4ba'),(7,7,1,'world',NULL,1,'2018-07-02 07:24:05','2018-07-02 07:24:05','f5c7557f-da1e-41a6-9349-743750924d4c'),(8,8,1,'s',NULL,1,'2018-07-02 07:25:06','2018-07-02 07:25:06','559104d6-7095-41c8-9414-444d9e4a2b3a'),(9,9,1,'spirit',NULL,1,'2018-07-02 07:25:14','2018-07-02 07:25:14','f69765b7-01d4-4870-aeec-ad09be48098d'),(10,10,1,'wine',NULL,1,'2018-07-02 07:25:16','2018-07-02 07:25:16','448d24a3-d69d-401e-9a73-16cc3a24090d'),(11,11,1,'champagne',NULL,1,'2018-07-02 07:25:20','2018-07-02 07:25:20','14fa173e-70ff-4881-9b42-ec7ae52eba11'),(12,12,1,'rival-coctail-bar','property-details/rival-coctail-bar',1,'2018-07-02 07:27:12','2018-07-06 07:29:45','5925ea92-af3a-408b-b1a1-8ee78c47a17f'),(13,13,1,NULL,NULL,1,'2018-07-02 07:27:12','2018-07-06 07:29:45','55f20982-2044-4533-b8cc-cc82b812737b'),(14,14,1,NULL,NULL,1,'2018-07-02 09:28:20','2018-07-02 10:12:08','2e80d6ca-71f8-4739-93c3-c4989767ac0f'),(15,15,1,NULL,NULL,1,'2018-07-02 10:12:03','2018-07-02 10:12:03','44b33656-0e5c-4069-86ef-21760cfa8643'),(16,16,1,NULL,NULL,1,'2018-07-02 10:15:24','2018-07-02 10:15:24','7fb86853-c27b-4fb7-9071-463eb3d499ff'),(17,17,1,NULL,NULL,1,'2018-07-02 10:23:23','2018-07-02 10:23:23','201c922f-0d07-4c7b-9680-0ee99bf3dd52'),(18,18,1,NULL,NULL,1,'2018-07-02 10:23:24','2018-07-02 10:23:24','c9fd50a0-13a1-4e6d-a6e6-a015e5da146c'),(19,19,1,'coca-cola-burger','property-details/coca-cola-burger',1,'2018-07-02 12:55:31','2018-07-06 07:29:45','2a249d77-d9a5-41e3-815c-8569b3999628'),(20,20,1,'burger',NULL,1,'2018-07-04 09:19:58','2018-07-04 09:19:58','b4553fa2-c9e6-4cb7-80cc-c7fe6c25d7fc'),(21,21,1,'bar',NULL,1,'2018-07-04 09:20:01','2018-07-04 09:20:01','6aa6801a-aa6c-41ae-92e2-b747eae63fc3'),(22,22,1,'cocktails',NULL,1,'2018-07-04 09:20:11','2018-07-04 09:20:11','fba11fa3-7c76-4480-b48e-1844db1bb581'),(23,23,1,'burger-bar','property-details/burger-bar',1,'2018-07-04 09:21:21','2018-07-06 07:29:45','d2c33290-c0bf-40a1-bf86-70526b05b3fb'),(24,24,1,NULL,NULL,1,'2018-07-04 09:21:21','2018-07-06 07:29:46','7a97f24a-f0e0-490a-894c-9f38f936fcf0'),(25,25,1,'hello','property-details/hello',1,'2018-07-04 09:29:41','2018-07-06 07:30:03','61a73980-bd0a-42f0-af06-d2c800f1ae0f'),(26,26,1,'hello-1','property-details/hello-1',1,'2018-07-06 11:06:19','2018-07-06 11:06:19','d992c24d-7c44-4ae8-b3fd-bdd0ebe96eaa'),(27,27,1,'hello-2','property-details/hello-2',1,'2018-07-06 11:06:30','2018-07-06 11:06:30','71fb2ed2-af30-4f58-9b2e-74ccdcd69851'),(28,28,1,'hello-2-1','property-details/hello-2-1',1,'2018-07-06 11:06:47','2018-07-06 11:06:47','99dd3fbf-bbcd-45c5-99b7-171759938252'),(29,29,1,'hello-2-1-1','property-details/hello-2-1-1',1,'2018-07-06 11:06:49','2018-07-06 11:06:50','cb1c8f6e-1a67-491d-a21b-40bd48a97002'),(30,30,1,'hello-2-1-1-1','property-details/hello-2-1-1-1',1,'2018-07-06 11:06:52','2018-07-06 11:06:52','f53789e2-d7a5-4336-ad20-338f7daada6a'),(31,31,1,'hello-2-1-1-1-1','property-details/hello-2-1-1-1-1',1,'2018-07-06 11:06:54','2018-07-06 11:06:54','fb80ba3b-ae97-4ef6-8a23-a02ec597bbe0'),(32,32,1,'hello-2-1-1-1-1-1','property-details/hello-2-1-1-1-1-1',1,'2018-07-06 11:06:57','2018-07-06 11:06:57','4c6238e8-d0af-486b-92fc-d7c57a082af8'),(33,33,1,'hello-2-1-1-1-1-1-1','property-details/hello-2-1-1-1-1-1-1',1,'2018-07-06 11:07:00','2018-07-06 11:07:00','c6978c87-112d-4f33-bd6e-ceea2665b889'),(34,34,1,'hello-2-1-1-1-1-1-1-1','property-details/hello-2-1-1-1-1-1-1-1',1,'2018-07-06 11:07:14','2018-07-06 11:07:15','53bbcad1-75fb-4bbf-baa5-d79510611b4c'),(35,35,1,'hello-2-1-1-1-1-1-1-1-1','property-details/hello-2-1-1-1-1-1-1-1-1',1,'2018-07-06 11:07:17','2018-07-06 11:07:17','c2d7798a-a1fe-4040-89f7-89b97bcca3b0'),(36,36,1,'burger-bar-1','property-details/burger-bar-1',1,'2018-07-06 11:07:29','2018-07-06 11:07:29','a2b9a84a-c7df-4fde-b3ad-4ba106e50dfc'),(37,37,1,NULL,NULL,1,'2018-07-06 11:07:29','2018-07-06 11:07:29','5b2ef399-2aa1-42d5-ab51-80be55ec2492'),(38,38,1,'burger-bar-1-1','property-details/burger-bar-1-1',1,'2018-07-06 11:07:32','2018-07-06 11:07:32','3f8ba6f5-e750-40f3-a848-07b3d46c2d13'),(39,39,1,NULL,NULL,1,'2018-07-06 11:07:32','2018-07-06 11:07:32','523c49eb-3de5-4cd9-a9d0-816e5f5a2ef9'),(40,40,1,'burger-bar-1-1-1','property-details/burger-bar-1-1-1',1,'2018-07-06 11:07:35','2018-07-06 11:07:35','2d22464d-eddc-4826-9cd0-fb20b5bde18a'),(41,41,1,NULL,NULL,1,'2018-07-06 11:07:35','2018-07-06 11:07:35','e633888c-0181-4b1e-a7c2-ce5fbbbc8a91'),(42,42,1,'burger-bar-1-1-1-1','property-details/burger-bar-1-1-1-1',1,'2018-07-06 11:07:39','2018-07-06 11:07:39','e311dab7-c319-4753-a116-98de897c25e4'),(43,43,1,NULL,NULL,1,'2018-07-06 11:07:39','2018-07-06 11:07:39','6a2218fb-d1c7-4161-b432-af3c9221c99a'),(44,44,1,'burger-bar-1-1-1-1-1','property-details/burger-bar-1-1-1-1-1',1,'2018-07-06 11:07:42','2018-07-06 11:07:42','6013c20e-9464-4aa6-a1dc-34f9b4f1108f'),(45,45,1,NULL,NULL,1,'2018-07-06 11:07:42','2018-07-06 11:07:42','e82117ee-0cb1-4638-a8ed-8873e130e8ce'),(46,46,1,'burger-bar-1-1-1-1-1-1','property-details/burger-bar-1-1-1-1-1-1',1,'2018-07-06 11:07:47','2018-07-06 11:07:47','5838ec7d-837f-4fff-bc60-98c8723e78bb'),(47,47,1,NULL,NULL,1,'2018-07-06 11:07:47','2018-07-06 11:07:47','5c5917a1-65c7-47cd-b7ab-96857e5c929b'),(48,48,1,'burger-bar-1-1-1-1-1-1-1','property-details/burger-bar-1-1-1-1-1-1-1',1,'2018-07-06 11:07:51','2018-07-06 11:07:51','bb64a43d-02c0-477d-a0b6-ea5397a9b99d'),(49,49,1,NULL,NULL,1,'2018-07-06 11:07:51','2018-07-06 11:07:51','c4829807-c98b-44af-86eb-4567e8ae1ca4');
/*!40000 ALTER TABLE `elements_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_idx` (`authorId`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
INSERT INTO `entries` VALUES (12,1,1,1,'2018-07-02 07:26:00',NULL,'2018-07-02 07:27:12','2018-07-06 07:29:45','6fb3ba27-abba-4fb1-a4be-d7464c6d68e4'),(19,1,1,1,'2018-07-02 12:55:00',NULL,'2018-07-02 12:55:31','2018-07-06 07:29:45','d592806f-1099-402c-9ac8-f18c7c7ede68'),(23,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-04 09:21:21','2018-07-06 07:29:46','1895381d-a1d9-427a-a057-69c4ad5055d9'),(25,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-04 09:29:41','2018-07-06 07:30:03','eabf1ec4-42ca-4036-85d4-ff5f32661c21'),(26,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:19','2018-07-06 11:06:19','cc1167b9-c7b8-4a70-8edc-5f10a2216024'),(27,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:30','2018-07-06 11:06:30','364b39fa-8e40-460e-a2a8-7cb2df69311e'),(28,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:47','2018-07-06 11:06:47','d93e9224-9d9b-4dd8-a2b8-d43364e37410'),(29,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:49','2018-07-06 11:06:50','72f433e1-f57c-45e6-9175-5fb2fc59384b'),(30,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:52','2018-07-06 11:06:52','5962a3a0-67c5-4d32-ae98-693e01f7d1d8'),(31,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:54','2018-07-06 11:06:54','080bc83e-169a-4263-ba8f-28e3a45d162f'),(32,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:06:57','2018-07-06 11:06:57','62a7e593-ae57-445c-aa95-7326eba842e9'),(33,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:07:00','2018-07-06 11:07:00','81054e72-58e1-4dd4-9ee3-4cbae3a4fdf1'),(34,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:07:15','2018-07-06 11:07:15','05a0b4a9-3a51-46b5-a0aa-62bb58ff6285'),(35,1,1,1,'2018-07-04 09:29:00',NULL,'2018-07-06 11:07:17','2018-07-06 11:07:17','405af73c-7f67-49df-94bc-f8150207896e'),(36,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:29','2018-07-06 11:07:29','8b8bfa88-1b54-4ef6-819c-6cb249dfd4e7'),(38,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:32','2018-07-06 11:07:32','e19e496c-d40d-4bcc-bc0b-45adcf061ef0'),(40,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:35','2018-07-06 11:07:35','0946444d-1def-41a3-99a1-4664678ec3f8'),(42,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:39','2018-07-06 11:07:39','e7b9b086-e86b-42f6-a2d8-8fbd7a5c0ecd'),(44,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:42','2018-07-06 11:07:42','78d86156-6a93-4815-a700-79495b89a99e'),(46,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:47','2018-07-06 11:07:47','5058054a-2b5b-4b06-b08b-ac7576a236ae'),(48,1,1,1,'2018-07-04 09:21:00',NULL,'2018-07-06 11:07:51','2018-07-06 11:07:51','bf1411ba-7605-41c7-b935-a01439723c9e');
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrydrafts`
--

DROP TABLE IF EXISTS `entrydrafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrydrafts_sectionId_idx` (`sectionId`),
  KEY `entrydrafts_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `entrydrafts_siteId_idx` (`siteId`),
  KEY `entrydrafts_creatorId_idx` (`creatorId`),
  CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrydrafts`
--

LOCK TABLES `entrydrafts` WRITE;
/*!40000 ALTER TABLE `entrydrafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrydrafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrytypes`
--

DROP TABLE IF EXISTS `entrytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_idx` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrytypes`
--

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;
INSERT INTO `entrytypes` VALUES (1,1,4,'Properties','properties',0,NULL,'{propertyName}',1,'2018-07-02 07:17:49','2018-07-06 07:29:41','526667c6-211f-4a6d-9b00-12deba6d628b');
/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entryversions`
--

DROP TABLE IF EXISTS `entryversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `siteId` int(11) NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entryversions_sectionId_idx` (`sectionId`),
  KEY `entryversions_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `entryversions_siteId_idx` (`siteId`),
  KEY `entryversions_creatorId_idx` (`creatorId`),
  CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entryversions`
--

LOCK TABLES `entryversions` WRITE;
/*!40000 ALTER TABLE `entryversions` DISABLE KEYS */;
INSERT INTO `entryversions` VALUES (1,12,1,1,1,1,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Rival Coctail Bar\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 07:27:12','2018-07-02 07:27:12','9ef5b642-1767-4911-8613-289cf8b44c71'),(2,12,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"New\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"14\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 09:30:54','2018-07-02 09:30:54','9bcaafce-715c-4a0f-814f-72270c047137'),(3,12,1,1,1,3,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"New\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"14\",\"15\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 10:12:08','2018-07-02 10:12:08','a05c4207-bdaa-4dc8-88a5-7dad9a7e2eb1'),(4,12,1,1,1,4,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"New\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitue\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"17\",\"18\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 10:25:14','2018-07-02 10:25:14','4f537b1c-3d44-402c-89c1-fac1ad9adcb1'),(5,12,1,1,1,5,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Rival Coctail Bar\",\"slug\":\"rival-coctail-bar\",\"postDate\":1530516360,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"4\",\"5\"],\"7\":{\"13\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"14.3423\",\"latitude\":\"35.2344\"}}},\"11\":[\"17\",\"18\"],\"5\":\"email@gmail.com\",\"10\":\"Gldanula, Tbilisi, Georgia\",\"1\":\"Rival Coctail Bar\",\"6\":\"2147483647\",\"4\":\"https://website.com\",\"2\":[\"11\",\"9\",\"10\"]}}','2018-07-02 12:55:18','2018-07-02 12:55:18','7f00d9a3-4755-47aa-8a32-3a58883d7d3a'),(6,19,1,1,1,1,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Coca Cola Burger\",\"slug\":\"coca-cola-burger\",\"postDate\":1530536100,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"11\":[],\"5\":\"\",\"1\":\"Coca Cola Burger\",\"6\":\"321432\",\"4\":\"\",\"2\":[]}}','2018-07-02 12:55:31','2018-07-02 12:55:31','17a977ee-1ac3-48fa-b170-0640c3486ed6'),(7,19,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Coca Cola Burger\",\"slug\":\"coca-cola-burger\",\"postDate\":1530536100,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"2\",\"3\",\"4\",\"5\"],\"7\":[],\"11\":[],\"5\":\"\",\"1\":\"Coca Cola Burger\",\"6\":\"321432\",\"4\":\"\",\"2\":[]}}','2018-07-03 07:53:29','2018-07-03 07:53:29','08e9764a-a462-4b1a-aa87-4c3fcbf56442'),(8,23,1,1,1,1,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"24\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"11\":[\"17\"],\"5\":\"mail@hotmail.com\",\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"4\":\"http://site.com\",\"2\":[\"21\",\"20\",\"22\"]}}','2018-07-04 09:21:21','2018-07-04 09:21:21','89e04787-eb6f-4164-bc1c-4d4b302fdc43'),(9,25,1,1,1,1,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"11\":[],\"5\":\"\",\"1\":\"Hello\",\"6\":\"342\",\"4\":\"\",\"2\":[]}}','2018-07-04 09:29:41','2018-07-04 09:29:41','d8ec5e9a-84ab-43a1-a6a0-f6ffee4ee728'),(10,25,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal\",\"11\":[],\"5\":\"\",\"1\":\"Hello\",\"6\":\"342\",\"4\":\"\",\"2\":[]}}','2018-07-06 07:30:03','2018-07-06 07:30:03','71ed1859-299f-464f-927b-0f74b85df379'),(11,26,1,1,1,1,'Revision from Jul 6, 2018, 3:06:19 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:19','2018-07-06 11:06:19','281595bf-09a1-453e-9a42-538d6c7082c0'),(12,26,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:19','2018-07-06 11:06:19','01409ac6-2a69-48a9-820f-0f91a528b25c'),(13,27,1,1,1,1,'Revision from Jul 6, 2018, 3:06:30 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:30','2018-07-06 11:06:30','c6237d33-0cc0-492e-b55c-8b97a82f0b50'),(14,27,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:30','2018-07-06 11:06:30','399bcc49-1ba5-40ac-b584-18ebe0475f95'),(15,28,1,1,1,1,'Revision from Jul 6, 2018, 3:06:47 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:47','2018-07-06 11:06:47','6f76dca6-0dfa-4a92-b499-7d55c8b41a90'),(16,28,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:47','2018-07-06 11:06:47','994f045d-b8e5-42fe-aa94-1f65fc88fbb0'),(17,29,1,1,1,1,'Revision from Jul 6, 2018, 3:06:49 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:50','2018-07-06 11:06:50','3e3af7dc-3814-4867-8ae7-6a99df0bd034'),(18,29,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:50','2018-07-06 11:06:50','21c2a3f5-8169-4000-a70b-e82d2b60eb13'),(19,30,1,1,1,1,'Revision from Jul 6, 2018, 3:06:52 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:52','2018-07-06 11:06:52','ca9b2d12-61ca-4ef8-a619-54b1c9f2bd3b'),(20,30,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:52','2018-07-06 11:06:52','623f8146-5a37-46d4-9061-795ece153d1d'),(21,31,1,1,1,1,'Revision from Jul 6, 2018, 3:06:54 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:54','2018-07-06 11:06:54','4c611f22-f9b9-418f-a452-6162876bded1'),(22,31,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:54','2018-07-06 11:06:54','b468475c-a9d6-4e95-aec9-10d9464e98cf'),(23,32,1,1,1,1,'Revision from Jul 6, 2018, 3:06:57 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:57','2018-07-06 11:06:57','20ad1be4-b5bc-4a88-b59e-ecb8cda00df1'),(24,32,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:06:57','2018-07-06 11:06:57','7acebc87-49d8-4653-b926-16fbc688e523'),(25,33,1,1,1,1,'Revision from Jul 6, 2018, 3:07:00 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:07:00','2018-07-06 11:07:00','f85794bc-1cae-4d5a-8d45-b2c144c04114'),(26,33,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:07:00','2018-07-06 11:07:00','14d75419-5d4a-40fe-b5af-77349aa559ad'),(27,34,1,1,1,1,'Revision from Jul 6, 2018, 3:07:14 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:07:15','2018-07-06 11:07:15','43124527-2f29-464b-8ad0-a495dfe58333'),(28,34,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:07:15','2018-07-06 11:07:15','0e9c6840-cc2f-40b5-8eff-6bdadbe618d0'),(29,35,1,1,1,1,'Revision from Jul 6, 2018, 3:07:17 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:07:17','2018-07-06 11:07:17','1fa970b9-a856-49c7-b023-2d68ddc307b5'),(30,35,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Hello\",\"slug\":\"hello-2-1-1-1-1-1-1-1-1\",\"postDate\":1530696540,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[],\"7\":[],\"14\":\"<p>fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal</p>\",\"5\":\"\",\"11\":[],\"1\":\"Hello\",\"6\":\"342\",\"2\":[]}}','2018-07-06 11:07:17','2018-07-06 11:07:17','d0641575-d68a-4f81-89f9-5f42d1e52348'),(31,36,1,1,1,1,'Revision from Jul 6, 2018, 3:07:29 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:29','2018-07-06 11:07:29','d43c87d8-f7d9-4ccd-8658-0e5954d4f9e4'),(32,36,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"37\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:29','2018-07-06 11:07:29','b9a09a44-14ff-450e-b1a2-286ed41b7706'),(33,38,1,1,1,1,'Revision from Jul 6, 2018, 3:07:32 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:32','2018-07-06 11:07:32','2d85479d-9b48-45d1-8f00-1557cf88cc25'),(34,38,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"39\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:32','2018-07-06 11:07:32','98c0065b-f2d3-41b4-b9a5-408e0597c4bd'),(35,40,1,1,1,1,'Revision from Jul 6, 2018, 3:07:35 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:35','2018-07-06 11:07:35','de9f8581-baf4-4535-b669-ece8e21b366c'),(36,40,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"41\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:35','2018-07-06 11:07:35','076f6abd-f836-40b4-917d-40d06fb61df9'),(37,42,1,1,1,1,'Revision from Jul 6, 2018, 3:07:39 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:39','2018-07-06 11:07:39','c64a1833-d55f-41f9-b440-18dd632d8d0a'),(38,42,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"43\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:39','2018-07-06 11:07:39','06714971-3929-4e5d-8fb8-a4afaade1b86'),(39,44,1,1,1,1,'Revision from Jul 6, 2018, 3:07:42 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:42','2018-07-06 11:07:42','d8dfdeae-3bbe-41d4-9542-5ac5ef5e02be'),(40,44,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"45\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:42','2018-07-06 11:07:42','6cc6a2cc-ae12-44fc-8edf-759ecb5eaea1'),(41,46,1,1,1,1,'Revision from Jul 6, 2018, 3:07:47 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:47','2018-07-06 11:07:47','8ad019eb-caa8-4920-8e15-76c9c856b700'),(42,46,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"47\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:47','2018-07-06 11:07:47','188568cb-86de-4c65-9ce9-d4b847b385ba'),(43,48,1,1,1,1,'Revision from Jul 6, 2018, 3:07:51 PM','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"3\":[],\"12\":\"Tbilisi\",\"7\":[],\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[],\"4\":\"http://site.com\"}}','2018-07-06 11:07:51','2018-07-06 11:07:51','bd317178-69da-4071-9f06-44f42d100913'),(44,48,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":\"1\",\"title\":\"Burger Bar\",\"slug\":\"burger-bar-1-1-1-1-1-1-1\",\"postDate\":1530696060,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"3\":[\"3\",\"5\"],\"12\":\"Tbilisi\",\"7\":{\"49\":{\"type\":\"coordinateDetails\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"longtitude\":\"12.34\",\"latitude\":\"11.53\"}}},\"13\":\"Saburtalo\",\"5\":\"mail@hotmail.com\",\"11\":[\"17\"],\"10\":\"Georgia\",\"1\":\"Burger Bar\",\"6\":\"78328932\",\"2\":[\"21\",\"20\",\"22\"],\"4\":\"http://site.com\"}}','2018-07-06 11:07:51','2018-07-06 11:07:51','a5470f38-897c-4a56-85b3-4c67c70cab69');
/*!40000 ALTER TABLE `entryversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldgroups`
--

DROP TABLE IF EXISTS `fieldgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldgroups`
--

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;
INSERT INTO `fieldgroups` VALUES (1,'Common','2018-07-02 06:38:30','2018-07-02 06:38:30','51136e91-ae9e-454a-bb10-47ad0602b7db'),(2,'Property Fields','2018-07-02 07:04:30','2018-07-06 07:41:17','7f51e00a-071d-4e43-8355-ab09a7e978e9');
/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayoutfields`
--

DROP TABLE IF EXISTS `fieldlayoutfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  KEY `fieldlayoutfields_fieldId_idx` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayoutfields`
--

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;
INSERT INTO `fieldlayoutfields` VALUES (51,1,11,8,0,1,'2018-07-02 10:31:07','2018-07-02 10:31:07','e7dc1325-37dc-41b4-8af5-694ff7886b72'),(52,1,11,9,0,2,'2018-07-02 10:31:07','2018-07-02 10:31:07','4e0025bd-297d-43d9-8049-0632d4448f1d'),(100,4,17,1,1,1,'2018-07-06 07:29:41','2018-07-06 07:29:41','f7a18a01-4475-4cd7-b285-f82a09410f9d'),(101,4,17,14,0,2,'2018-07-06 07:29:41','2018-07-06 07:29:41','b94bbc79-1bd9-4f70-9eb2-5e64f386d306'),(102,4,17,2,0,3,'2018-07-06 07:29:41','2018-07-06 07:29:41','c7b76f29-421b-44d8-a7b3-54e22b712b22'),(103,4,17,3,0,4,'2018-07-06 07:29:41','2018-07-06 07:29:41','40645585-2829-42cf-9217-e833a3bd5f28'),(104,4,17,11,0,5,'2018-07-06 07:29:41','2018-07-06 07:29:41','98418c5d-2951-49da-995b-a32740e3d830'),(105,4,17,6,1,6,'2018-07-06 07:29:41','2018-07-06 07:29:41','0d11ff7d-a800-4867-81b2-44ffae6b2451'),(106,4,17,4,0,7,'2018-07-06 07:29:41','2018-07-06 07:29:41','d462bed8-a6cc-4d15-9904-adb3243b396e'),(107,4,17,5,0,8,'2018-07-06 07:29:41','2018-07-06 07:29:41','54ac0816-0b70-4480-ac1a-08601640e88c'),(108,4,17,10,0,9,'2018-07-06 07:29:41','2018-07-06 07:29:41','b849f928-818b-47b3-aa2e-7d1d2e6bbe77'),(109,4,17,12,0,10,'2018-07-06 07:29:41','2018-07-06 07:29:41','bb052875-c028-4375-9549-8ced223c7e7b'),(110,4,17,13,0,11,'2018-07-06 07:29:41','2018-07-06 07:29:41','8d9a7dac-4480-4a48-baf4-4784c8495985'),(111,4,17,7,0,12,'2018-07-06 07:29:41','2018-07-06 07:29:41','cf952e88-cfb3-41e6-92fe-17b6812c5c08');
/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouts`
--

DROP TABLE IF EXISTS `fieldlayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouts`
--

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;
INSERT INTO `fieldlayouts` VALUES (1,'craft\\elements\\MatrixBlock','2018-07-02 07:12:06','2018-07-02 10:31:07','fb15d6e3-2165-40aa-8db5-8863e31bacb6'),(2,'craft\\elements\\Category','2018-07-02 07:14:24','2018-07-02 07:22:05','c763cb74-3a3b-4e37-826e-ec92dc904db7'),(3,'craft\\elements\\Tag','2018-07-02 07:15:38','2018-07-02 07:24:27','812afc38-bae2-45bd-8f65-fb10e33d0b9a'),(4,'craft\\elements\\Entry','2018-07-02 07:17:49','2018-07-06 07:29:41','55bb8343-58e2-45e7-8498-198ba7b97d2e'),(5,'craft\\elements\\Asset','2018-07-02 09:27:30','2018-07-02 10:26:05','2dfff8ec-e5fb-4471-ad42-14952c7d13ce');
/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouttabs`
--

DROP TABLE IF EXISTS `fieldlayouttabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_idx` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouttabs`
--

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;
INSERT INTO `fieldlayouttabs` VALUES (11,1,'Content',1,'2018-07-02 10:31:07','2018-07-02 10:31:07','5fc2021f-2507-456d-b23b-058afa0aa3fe'),(17,4,'Property Details',1,'2018-07-06 07:29:41','2018-07-06 07:29:41','33595590-7a22-468f-bcab-07cbf859568b');
/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_groupId_idx` (`groupId`),
  KEY `fields_context_idx` (`context`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES (1,2,'Name','propertyName','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:05:39','2018-07-06 07:37:18','223f759e-5dbc-49ea-bd0c-60bd30455489'),(2,2,'Tags','tags','global','','site',NULL,'craft\\fields\\Tags','{\"sources\":\"*\",\"source\":\"taggroup:1\",\"targetSiteId\":null,\"viewMode\":null,\"limit\":null,\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-07-02 07:06:23','2018-07-02 07:23:51','de9dfafb-559a-4f6b-9266-b9781b2be583'),(3,2,'Categories','categories','global','','site',NULL,'craft\\fields\\Categories','{\"branchLimit\":\"\",\"sources\":\"*\",\"source\":\"group:1\",\"targetSiteId\":null,\"viewMode\":null,\"limit\":null,\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-07-02 07:07:49','2018-07-02 07:14:37','6d7b8438-3e3f-4d1f-a304-e22365497714'),(4,2,'URL','propertyURL','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:08:19','2018-07-06 07:40:49','e120e90e-5136-49a7-b9c3-2c5518ab50fc'),(5,2,'Email','propertyEmail','global','','none',NULL,'craft\\fields\\Email','[]','2018-07-02 07:08:43','2018-07-06 07:36:58','700e58d3-9f3f-4889-897f-e95600e95431'),(6,2,'Number','propertyNumber','global','','none',NULL,'craft\\fields\\Number','{\"defaultValue\":null,\"min\":\"0\",\"max\":null,\"decimals\":0,\"size\":null}','2018-07-02 07:10:04','2018-07-06 07:37:27','87845b7b-490d-4d4d-a146-4c6b67945935'),(7,2,'Coordinates','coordinates','global','','site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"localizeBlocks\":false}','2018-07-02 07:12:05','2018-07-02 10:31:07','e6ca888e-9795-4080-bc3d-f08a1022ad61'),(8,NULL,'Longtitude','longtitude','matrixBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:12:06','2018-07-02 10:31:07','e3158136-e0f3-4915-9858-d4cce039e963'),(9,NULL,'Latitude','latitude','matrixBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:12:06','2018-07-02 10:31:07','e4289f90-e5c2-46c5-af5c-b06222d91f75'),(10,2,'Location','propertyLocation','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-02 07:12:42','2018-07-06 07:37:10','7a89ab2b-1e48-4258-9e06-9c3e63beb1d5'),(11,2,'Images','images','global','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"1\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"/assets\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-07-02 09:21:39','2018-07-02 10:11:47','3420a3fc-2f64-4f0d-b8e8-0830c7d21c8a'),(12,2,'City','city','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-04 09:18:32','2018-07-04 09:19:09','6c173c27-c1c7-4f99-a185-6c4c89d83f53'),(13,2,'District','district','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-07-04 09:19:20','2018-07-04 09:19:20','05abe1d5-c95d-450c-adf4-433e31586da4'),(14,2,'Description','description','global','','none',NULL,'froala\\craftfroalawysiwyg\\Field','{\"assetsImagesSource\":\"\",\"assetsImagesSubPath\":\"\",\"assetsFilesSource\":\"\",\"assetsFilesSubPath\":\"\",\"editorConfig\":\"\"}','2018-07-06 07:26:15','2018-07-06 07:36:20','795cbb6a-9010-4b53-b0e2-f12d621064dd');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `globalsets`
--

DROP TABLE IF EXISTS `globalsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `globalsets`
--

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;
/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `edition` tinyint(3) unsigned NOT NULL,
  `timezone` varchar(30) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `on` tinyint(1) NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` VALUES (1,'3.0.13.2','3.0.91',0,'Asia/Tbilisi','Absolut',1,0,'LZ0XJUZCQ3gN','2018-07-02 06:38:30','2018-07-06 07:40:49','9effd330-c2d5-47a7-a96e-c3bc233c1c8f');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocks`
--

DROP TABLE IF EXISTS `matrixblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `matrixblocks_ownerSiteId_idx` (`ownerSiteId`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocks`
--

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;
INSERT INTO `matrixblocks` VALUES (13,12,NULL,7,1,1,'2018-07-02 07:27:12','2018-07-06 07:29:45','ed0c83ab-62fe-4e51-89e6-463d9bf8ae9e'),(24,23,NULL,7,1,1,'2018-07-04 09:21:21','2018-07-06 07:29:46','d4c3285d-c1a4-444f-a2f1-25a8d6c4cadf'),(37,36,NULL,7,1,1,'2018-07-06 11:07:29','2018-07-06 11:07:29','676abb49-5d57-4fe0-b989-803f29f108fe'),(39,38,NULL,7,1,1,'2018-07-06 11:07:32','2018-07-06 11:07:32','21a2bab4-96d0-4e8f-9796-49d768b90f1c'),(41,40,NULL,7,1,1,'2018-07-06 11:07:35','2018-07-06 11:07:35','5a3e8838-8260-44f3-8710-53ef6fc022ba'),(43,42,NULL,7,1,1,'2018-07-06 11:07:39','2018-07-06 11:07:39','7409cc89-c9e5-4608-8736-0425ab0ed379'),(45,44,NULL,7,1,1,'2018-07-06 11:07:42','2018-07-06 11:07:42','1b055979-c085-49fb-bcb5-f6c464d238f3'),(47,46,NULL,7,1,1,'2018-07-06 11:07:47','2018-07-06 11:07:47','28d3ea15-2e31-4430-9df2-a8c71e9a3848'),(49,48,NULL,7,1,1,'2018-07-06 11:07:51','2018-07-06 11:07:51','a0c8ad70-d3be-42c0-afec-2beebc42b5ce');
/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocktypes`
--

DROP TABLE IF EXISTS `matrixblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocktypes`
--

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;
INSERT INTO `matrixblocktypes` VALUES (1,7,1,'CoordinateDetails','coordinateDetails',1,'2018-07-02 07:12:06','2018-07-02 10:31:07','578113b2-3c50-4223-87c7-130c131fc2f5');
/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixcontent_coordinates`
--

DROP TABLE IF EXISTS `matrixcontent_coordinates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_coordinates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_coordinateDetails_longtitude` text,
  `field_coordinateDetails_latitude` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_coordinates_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_coordinates_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_coordinates_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_coordinates_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixcontent_coordinates`
--

LOCK TABLES `matrixcontent_coordinates` WRITE;
/*!40000 ALTER TABLE `matrixcontent_coordinates` DISABLE KEYS */;
INSERT INTO `matrixcontent_coordinates` VALUES (1,13,1,'2018-07-02 07:27:12','2018-07-06 07:29:45','d622d6f4-895b-4c12-9424-d16128b20f6d','14.3423','35.2344'),(2,24,1,'2018-07-04 09:21:21','2018-07-06 07:29:46','c559275a-4381-458a-b501-dae68d6f7d5d','12.34','11.53'),(3,37,1,'2018-07-06 11:07:29','2018-07-06 11:07:29','ef2db5db-2438-4ddc-a317-496d63834c7c','12.34','11.53'),(4,39,1,'2018-07-06 11:07:32','2018-07-06 11:07:32','76eb86d6-308c-4d61-b008-60ac454b1793','12.34','11.53'),(5,41,1,'2018-07-06 11:07:35','2018-07-06 11:07:35','428d23a6-4d66-4939-a26b-0e4c720dc141','12.34','11.53'),(6,43,1,'2018-07-06 11:07:39','2018-07-06 11:07:39','aa79814c-f292-4ab3-be0d-918265763432','12.34','11.53'),(7,45,1,'2018-07-06 11:07:42','2018-07-06 11:07:42','11141bf0-eaf6-44e3-b38b-3952881b11bc','12.34','11.53'),(8,47,1,'2018-07-06 11:07:47','2018-07-06 11:07:47','83e7a6e8-a14b-4454-85d1-9ff7d6eddd87','12.34','11.53'),(9,49,1,'2018-07-06 11:07:51','2018-07-06 11:07:51','1edca6c8-d6e7-4998-89a5-91511c8e5f7d','12.34','11.53');
/*!40000 ALTER TABLE `matrixcontent_coordinates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `migrations_pluginId_idx` (`pluginId`),
  KEY `migrations_type_pluginId_idx` (`type`,`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,NULL,'app','Install','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','bbe31123-0301-4855-9169-d5911014bf1b'),(2,NULL,'app','m150403_183908_migrations_table_changes','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','9059de25-dc4c-4160-bfe6-764d026ff437'),(3,NULL,'app','m150403_184247_plugins_table_changes','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','859cc316-6728-4786-b2cf-ea6a9d7c13e9'),(4,NULL,'app','m150403_184533_field_version','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','167e6f0c-f145-4fa1-87ab-ee8c6d9613f9'),(5,NULL,'app','m150403_184729_type_columns','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','c06e0052-e37d-449c-a44a-954e5869699c'),(6,NULL,'app','m150403_185142_volumes','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','78120c5b-c11f-46ae-86e2-f9e9a5be88ba'),(7,NULL,'app','m150428_231346_userpreferences','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','a46a5b4c-2958-41e5-993d-94d19f119b6e'),(8,NULL,'app','m150519_150900_fieldversion_conversion','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','a0563b96-52ab-4c27-a5e0-64115edf1ca6'),(9,NULL,'app','m150617_213829_update_email_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','3fbd8225-57ef-40b9-a6ed-3111ed6a82e5'),(10,NULL,'app','m150721_124739_templatecachequeries','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f077bed9-1ae7-405b-a78b-3718dc934913'),(11,NULL,'app','m150724_140822_adjust_quality_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','309303f4-4d22-4fd2-ae5c-6486cae3ec61'),(12,NULL,'app','m150815_133521_last_login_attempt_ip','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','0ddadfe9-6e13-4a49-932b-a19721a47446'),(13,NULL,'app','m151002_095935_volume_cache_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','2a96c2b1-759d-452d-881f-d44fab5c1bf1'),(14,NULL,'app','m151005_142750_volume_s3_storage_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','630316ed-c1d5-4e82-b282-3e56197ab2ba'),(15,NULL,'app','m151016_133600_delete_asset_thumbnails','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','96fe5ae2-231b-4e3c-a3ee-e2ed3b26ca7e'),(16,NULL,'app','m151209_000000_move_logo','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f28b144b-969f-4f30-933f-50da757a57cc'),(17,NULL,'app','m151211_000000_rename_fileId_to_assetId','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','5e8420ca-d1d1-43ae-9c6d-6674379d33c9'),(18,NULL,'app','m151215_000000_rename_asset_permissions','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','5c6c796f-4e82-4818-a883-9c9198f952ef'),(19,NULL,'app','m160707_000001_rename_richtext_assetsource_setting','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','a79b409b-bbbd-46b6-90db-ee1dabbad3c1'),(20,NULL,'app','m160708_185142_volume_hasUrls_setting','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','b3204ae5-95d9-4a75-b0d8-eaed2a6ee161'),(21,NULL,'app','m160714_000000_increase_max_asset_filesize','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','9439c41a-d35b-483f-83db-d43a12fa50f9'),(22,NULL,'app','m160727_194637_column_cleanup','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f40d0098-d26e-4795-bc69-47a22217cfdf'),(23,NULL,'app','m160804_110002_userphotos_to_assets','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','3fbc9b77-12c0-470d-9974-277a14e66a56'),(24,NULL,'app','m160807_144858_sites','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','0247155d-01c1-4309-8d98-65b648907054'),(25,NULL,'app','m160829_000000_pending_user_content_cleanup','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','6bc2aa5e-641c-4c60-a593-98e370c4f5df'),(26,NULL,'app','m160830_000000_asset_index_uri_increase','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','32ccabc5-285c-4b3c-99fe-b3a6208e0220'),(27,NULL,'app','m160912_230520_require_entry_type_id','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','e8fedd6e-2698-44ed-b4f7-f1ec31d28f7c'),(28,NULL,'app','m160913_134730_require_matrix_block_type_id','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','858744c8-819b-4c09-b254-c3d28a4af691'),(29,NULL,'app','m160920_174553_matrixblocks_owner_site_id_nullable','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','e4b49820-b718-4b71-8e9b-de5da171a7cd'),(30,NULL,'app','m160920_231045_usergroup_handle_title_unique','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','1d9657f2-41f0-4004-b12f-dfd5e9874e32'),(31,NULL,'app','m160925_113941_route_uri_parts','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','f14162bc-fafd-42a9-b3f1-b25def8bf3e6'),(32,NULL,'app','m161006_205918_schemaVersion_not_null','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','2bc31fd6-17ee-4970-90f1-6610c4f4ca41'),(33,NULL,'app','m161007_130653_update_email_settings','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','c9f6044f-58f9-4da0-b184-9cce686b5731'),(34,NULL,'app','m161013_175052_newParentId','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','58293263-c2f4-4248-bf2e-31eb60edc855'),(35,NULL,'app','m161021_102916_fix_recent_entries_widgets','2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-02 06:38:31','d1545ef7-7f15-4fee-a46c-46ea6fcf2a81'),(36,NULL,'app','m161021_182140_rename_get_help_widget','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c918e791-6665-4c9c-8448-485890483c63'),(37,NULL,'app','m161025_000000_fix_char_columns','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','0028dc97-bed9-49e6-87c2-174a15622e6d'),(38,NULL,'app','m161029_124145_email_message_languages','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','13d739fd-dd20-4766-a56d-960e01f91cb7'),(39,NULL,'app','m161108_000000_new_version_format','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','93858e0d-02e9-4105-a18b-81f74f8d8eaa'),(40,NULL,'app','m161109_000000_index_shuffle','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','22a52ec1-7e6f-4618-b87e-aee883c9725a'),(41,NULL,'app','m161122_185500_no_craft_app','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a1c080a8-515c-4eef-b2ac-d6a74c0f8294'),(42,NULL,'app','m161125_150752_clear_urlmanager_cache','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','26b44ee3-f142-4f50-97b8-5db4bfd79606'),(43,NULL,'app','m161220_000000_volumes_hasurl_notnull','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a153d6c6-e90f-4007-bf38-26e1b832e8d9'),(44,NULL,'app','m170114_161144_udates_permission','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2f0398de-50bc-4507-bdfc-55aa89aa09e1'),(45,NULL,'app','m170120_000000_schema_cleanup','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','3aaa5f3b-9b02-4c17-85b1-f0813c7db8f9'),(46,NULL,'app','m170126_000000_assets_focal_point','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c7421fa9-d110-4a47-881e-f4d284932b9f'),(47,NULL,'app','m170206_142126_system_name','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','818dcf8b-8745-46b0-b4d6-486d2f7973e9'),(48,NULL,'app','m170217_044740_category_branch_limits','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','30240d4d-6c8b-4e52-9e94-0614391b4a02'),(49,NULL,'app','m170217_120224_asset_indexing_columns','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','f31ba712-2e96-481a-8257-51b930a28433'),(50,NULL,'app','m170223_224012_plain_text_settings','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','47ded7be-7eb0-4767-b728-f8a36e1227ba'),(51,NULL,'app','m170227_120814_focal_point_percentage','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2095ba2b-cf3a-48c5-a0e0-7b009b4abb1d'),(52,NULL,'app','m170228_171113_system_messages','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c9ed0598-c23d-40f1-8c3c-4cf4a53aae78'),(53,NULL,'app','m170303_140500_asset_field_source_settings','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','0b75f402-6ad2-41de-a6bd-1d541e2e1d4c'),(54,NULL,'app','m170306_150500_asset_temporary_uploads','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','216b7f3f-29ff-4948-86a2-10bb54817a56'),(55,NULL,'app','m170414_162429_rich_text_config_setting','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','05f0e51f-e62e-44cc-a901-f0c11e6d4088'),(56,NULL,'app','m170523_190652_element_field_layout_ids','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','f2a87323-25fe-4d2a-bc9b-2a0341f7b6f2'),(57,NULL,'app','m170612_000000_route_index_shuffle','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','fa1e96ae-ef43-4d6b-9043-0ebf657c16bd'),(58,NULL,'app','m170621_195237_format_plugin_handles','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','845c5151-14ad-4a50-bbce-0946f358cc8f'),(59,NULL,'app','m170630_161028_deprecation_changes','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','64b44c70-6180-480a-ab2f-51616653e584'),(60,NULL,'app','m170703_181539_plugins_table_tweaks','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','df1d0c29-f86c-4054-b8df-b52c8b7260cf'),(61,NULL,'app','m170704_134916_sites_tables','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','f0eb3a19-4d95-429e-b80f-f602e65d9a23'),(62,NULL,'app','m170706_183216_rename_sequences','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2509d865-4238-46bc-8728-62c113bad8b1'),(63,NULL,'app','m170707_094758_delete_compiled_traits','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2d8bfd64-4121-4256-abc4-152b421d1b74'),(64,NULL,'app','m170731_190138_drop_asset_packagist','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','20608848-f911-4381-9873-cdac501a59bf'),(65,NULL,'app','m170810_201318_create_queue_table','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','7ded959a-5f35-4396-993d-213de01d94c1'),(66,NULL,'app','m170816_133741_delete_compiled_behaviors','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c632fd3b-c1b4-49a9-a38e-d9283f3e4afe'),(67,NULL,'app','m170821_180624_deprecation_line_nullable','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','3a07255f-2792-4247-b318-bbffc45bd597'),(68,NULL,'app','m170903_192801_longblob_for_queue_jobs','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','0bd1bfc2-83c0-434d-a6da-7e9e6b08070c'),(69,NULL,'app','m170914_204621_asset_cache_shuffle','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','3f962f1d-2d38-40f5-80b3-762bb3f26ec1'),(70,NULL,'app','m171011_214115_site_groups','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','aeef3a31-98f7-4c9b-a43c-e810dd4587d3'),(71,NULL,'app','m171012_151440_primary_site','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','4d5604f4-221e-486c-b499-0ffa3c48bdda'),(72,NULL,'app','m171013_142500_transform_interlace','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a1ad570e-0253-441f-85ea-556341662de1'),(73,NULL,'app','m171016_092553_drop_position_select','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','522e7151-e574-423b-a678-fc274fd115b1'),(74,NULL,'app','m171016_221244_less_strict_translation_method','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','9609df66-4210-4c5e-96b7-b78109ec6d92'),(75,NULL,'app','m171107_000000_assign_group_permissions','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','dd05853c-a756-486f-81ba-41ddc53b0e81'),(76,NULL,'app','m171117_000001_templatecache_index_tune','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','5f8c0191-b4eb-4704-b8b5-dc298096be29'),(77,NULL,'app','m171126_105927_disabled_plugins','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','c75e2d46-f545-4f59-b4b5-5e57a8f393c8'),(78,NULL,'app','m171130_214407_craftidtokens_table','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','a8248e18-f7de-4bd1-86bc-1d2c907e7a4c'),(79,NULL,'app','m171202_004225_update_email_settings','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','6dda8fb6-198c-4d94-a3cc-9a0b055b8146'),(80,NULL,'app','m171204_000001_templatecache_index_tune_deux','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','b8e83468-b16e-4213-9923-78702d77e586'),(81,NULL,'app','m171205_130908_remove_craftidtokens_refreshtoken_column','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','388882fa-339c-4fb4-8876-e29e71383b68'),(82,NULL,'app','m171218_143135_longtext_query_column','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','650dd5c2-b420-4c93-a13f-d4c931c3a52c'),(83,NULL,'app','m171231_055546_environment_variables_to_aliases','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','6a6be0e7-1961-4727-9a76-303704db135c'),(84,NULL,'app','m180113_153740_drop_users_archived_column','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','68f9913b-35ec-4ab7-a0a8-d0ce14cdb074'),(85,NULL,'app','m180122_213433_propagate_entries_setting','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','71fa4925-c60a-441a-bbde-6ecac6dd8bb0'),(86,NULL,'app','m180124_230459_fix_propagate_entries_values','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','cd935bb2-311d-455c-8ea2-f98989953972'),(87,NULL,'app','m180128_235202_set_tag_slugs','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','7c10cb78-ca19-41a2-8f7f-30f5bfe791dd'),(88,NULL,'app','m180202_185551_fix_focal_points','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','d9f2a663-f265-4ab7-b86e-ee31c869e8be'),(89,NULL,'app','m180217_172123_tiny_ints','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','fa54358d-baf1-43da-b4fe-2de0105576a6'),(90,NULL,'app','m180321_233505_small_ints','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','cddab551-4633-4c1a-a348-2d2f71ca52e8'),(91,NULL,'app','m180328_115523_new_license_key_statuses','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','619eaec5-fd85-4424-abd8-e0b74020f510'),(92,NULL,'app','m180404_182320_edition_changes','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','737bface-44e1-436f-9a17-0c50b95f03b2'),(93,NULL,'app','m180411_102218_fix_db_routes','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','7955ac79-e2d4-4b09-94c3-b28f15b2ed90'),(94,NULL,'app','m180416_205628_resourcepaths_table','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','24f13ea5-d94f-4333-a539-5c39da2bc0b0'),(95,NULL,'app','m180418_205713_widget_cleanup','2018-07-02 06:38:32','2018-07-02 06:38:32','2018-07-02 06:38:32','2dec5c04-9964-4e31-82d2-bb3d35e5cfe8');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKey` char(24) DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugins_handle_unq_idx` (`handle`),
  KEY `plugins_enabled_idx` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
INSERT INTO `plugins` VALUES (2,'element-api','2.5.3','1.0.0',NULL,'unknown',1,NULL,'2018-07-02 06:56:30','2018-07-02 06:56:30','2018-07-07 12:04:51','f7dd141e-c85c-4d36-8949-67bd4eac754b'),(3,'froala-editor','2.8.1-rc.3','1.0.0',NULL,'unknown',1,NULL,'2018-07-06 07:35:53','2018-07-06 07:35:53','2018-07-07 12:04:51','9a87be91-0d96-4dfa-9142-59b477feecca');
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  KEY `queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `relations_sourceId_idx` (`sourceId`),
  KEY `relations_targetId_idx` (`targetId`),
  KEY `relations_sourceSiteId_idx` (`sourceSiteId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relations`
--

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
INSERT INTO `relations` VALUES (110,2,12,NULL,11,1,'2018-07-06 07:29:45','2018-07-06 07:29:45','dcfcba5a-af34-42fe-8458-437356f575b4'),(111,2,12,NULL,9,2,'2018-07-06 07:29:45','2018-07-06 07:29:45','e9a979e0-d514-41d4-8499-3699a39a6fe7'),(112,2,12,NULL,10,3,'2018-07-06 07:29:45','2018-07-06 07:29:45','60e7ef20-d219-4b64-b090-c269f1020abf'),(113,3,12,NULL,3,1,'2018-07-06 07:29:45','2018-07-06 07:29:45','c3289ca8-375b-458e-9731-87ce7825c50d'),(114,3,12,NULL,4,2,'2018-07-06 07:29:45','2018-07-06 07:29:45','e7dfe180-d05e-4770-8d67-264bfb888699'),(115,3,12,NULL,5,3,'2018-07-06 07:29:45','2018-07-06 07:29:45','d94e6d61-614d-4494-b08f-372f00caa640'),(116,11,12,NULL,17,1,'2018-07-06 07:29:45','2018-07-06 07:29:45','2c2351b6-bda2-4ac5-9908-e09a1fed395a'),(117,11,12,NULL,18,2,'2018-07-06 07:29:45','2018-07-06 07:29:45','5a99a7c0-2a67-4a6a-aa93-6b0a6979bac1'),(118,3,19,NULL,2,1,'2018-07-06 07:29:45','2018-07-06 07:29:45','439b5071-42a0-46f5-887b-f4656065d71b'),(119,3,19,NULL,3,2,'2018-07-06 07:29:45','2018-07-06 07:29:45','b618b428-0446-4a50-9609-812ae9391012'),(120,3,19,NULL,4,3,'2018-07-06 07:29:45','2018-07-06 07:29:45','f0ffe227-2c14-4965-9407-22c503c1848d'),(121,3,19,NULL,5,4,'2018-07-06 07:29:45','2018-07-06 07:29:45','069b5bdc-7627-4345-96ca-3cad2ee2074f'),(122,2,23,NULL,20,1,'2018-07-06 07:29:46','2018-07-06 07:29:46','b3299653-113a-47cd-95a5-d1b9077fcce2'),(123,2,23,NULL,21,2,'2018-07-06 07:29:46','2018-07-06 07:29:46','657be2b5-5829-47ec-9004-722f8ed12750'),(124,2,23,NULL,22,3,'2018-07-06 07:29:46','2018-07-06 07:29:46','3d60b18e-b202-4f30-8ed9-b7cbb5c8e74e'),(125,3,23,NULL,3,1,'2018-07-06 07:29:46','2018-07-06 07:29:46','dbb8622c-4152-488b-943e-b67b08e0e4ea'),(126,3,23,NULL,5,2,'2018-07-06 07:29:46','2018-07-06 07:29:46','35514821-84f0-4988-969c-f0808d0fd567'),(127,11,23,NULL,17,1,'2018-07-06 07:29:46','2018-07-06 07:29:46','194d34de-b19e-4ce4-878c-67a4d20b6b69'),(128,2,36,NULL,20,1,'2018-07-06 11:07:29','2018-07-06 11:07:29','11eabb62-7dcc-450c-9349-e2281ae2591f'),(129,2,36,NULL,21,2,'2018-07-06 11:07:29','2018-07-06 11:07:29','7c76f427-7429-489b-969b-71bea7f671a5'),(130,2,36,NULL,22,3,'2018-07-06 11:07:29','2018-07-06 11:07:29','6caac08b-3126-49af-9281-d0bb8462a548'),(131,3,36,NULL,3,1,'2018-07-06 11:07:29','2018-07-06 11:07:29','e71c4ac0-877f-4484-8dc4-842445b89a9c'),(132,3,36,NULL,5,2,'2018-07-06 11:07:29','2018-07-06 11:07:29','f4a9916e-6f2e-47be-9105-748aec23eb2a'),(133,11,36,NULL,17,1,'2018-07-06 11:07:29','2018-07-06 11:07:29','1e5d9d81-7c9f-4f2d-b8dd-322de9269ebd'),(134,2,38,NULL,20,1,'2018-07-06 11:07:32','2018-07-06 11:07:32','39f74314-77cd-405c-b7c6-399badc6ba78'),(135,2,38,NULL,21,2,'2018-07-06 11:07:32','2018-07-06 11:07:32','68bd495b-a9c0-4acf-8b17-e9ef28a402aa'),(136,2,38,NULL,22,3,'2018-07-06 11:07:32','2018-07-06 11:07:32','4fd2b85e-ce4b-4886-b116-a9bb0b0f1e16'),(137,3,38,NULL,3,1,'2018-07-06 11:07:32','2018-07-06 11:07:32','bfbe00e5-d061-4639-b64d-3ae0b03e746c'),(138,3,38,NULL,5,2,'2018-07-06 11:07:32','2018-07-06 11:07:32','8df520f6-ac7b-4e27-8a4a-5eb4cee7fc41'),(139,11,38,NULL,17,1,'2018-07-06 11:07:32','2018-07-06 11:07:32','8740a685-2219-4eb5-aafc-a966538e56e3'),(140,2,40,NULL,20,1,'2018-07-06 11:07:35','2018-07-06 11:07:35','16dea309-b6af-4708-80f6-75c3e97bdc34'),(141,2,40,NULL,21,2,'2018-07-06 11:07:35','2018-07-06 11:07:35','b2286f34-9efb-48b6-9dcd-0efe413bdd3c'),(142,2,40,NULL,22,3,'2018-07-06 11:07:35','2018-07-06 11:07:35','006792c4-3c5d-474b-b65b-4747d914c404'),(143,3,40,NULL,3,1,'2018-07-06 11:07:35','2018-07-06 11:07:35','cf32dd2a-585a-41e7-ae6b-2906de150c8b'),(144,3,40,NULL,5,2,'2018-07-06 11:07:35','2018-07-06 11:07:35','b290f6b3-ace7-4ede-b25c-26f9491cba41'),(145,11,40,NULL,17,1,'2018-07-06 11:07:35','2018-07-06 11:07:35','2fce6476-85df-45ed-b60e-0e6e5a9722c2'),(146,2,42,NULL,20,1,'2018-07-06 11:07:39','2018-07-06 11:07:39','d05b1e97-5177-4022-9c4b-88e9a2f14313'),(147,2,42,NULL,21,2,'2018-07-06 11:07:39','2018-07-06 11:07:39','d214a75f-95b8-4461-a107-7d95a69bd643'),(148,2,42,NULL,22,3,'2018-07-06 11:07:39','2018-07-06 11:07:39','1bdc005b-aa5c-4173-b96f-42bb17fd6683'),(149,3,42,NULL,3,1,'2018-07-06 11:07:39','2018-07-06 11:07:39','fb92ef0a-0393-44b1-a202-2b014ac90b95'),(150,3,42,NULL,5,2,'2018-07-06 11:07:39','2018-07-06 11:07:39','2b6ce7d3-59c6-47a5-9e03-aac5322bcd83'),(151,11,42,NULL,17,1,'2018-07-06 11:07:39','2018-07-06 11:07:39','9e80e953-97ef-4cc4-a7d1-58576fefed76'),(152,2,44,NULL,20,1,'2018-07-06 11:07:42','2018-07-06 11:07:42','cb3b97ba-ed40-42b7-b3f6-0f0bc365ad57'),(153,2,44,NULL,21,2,'2018-07-06 11:07:42','2018-07-06 11:07:42','ad78dd57-daef-404f-b3a3-01e729928184'),(154,2,44,NULL,22,3,'2018-07-06 11:07:42','2018-07-06 11:07:42','e08709f4-b12a-481b-9a09-65247a527fb2'),(155,3,44,NULL,3,1,'2018-07-06 11:07:42','2018-07-06 11:07:42','8db68326-87b8-4d64-8c8a-9ef09864ec9f'),(156,3,44,NULL,5,2,'2018-07-06 11:07:42','2018-07-06 11:07:42','0c679d46-6a62-4785-93df-8ed3c3501d4f'),(157,11,44,NULL,17,1,'2018-07-06 11:07:42','2018-07-06 11:07:42','5eb0791f-86e2-4219-8976-70343be0ba7e'),(158,2,46,NULL,20,1,'2018-07-06 11:07:47','2018-07-06 11:07:47','02999df1-dae1-4e57-962b-0fe1ed869fd2'),(159,2,46,NULL,21,2,'2018-07-06 11:07:47','2018-07-06 11:07:47','b6028605-6653-400a-81dc-aff011f0704e'),(160,2,46,NULL,22,3,'2018-07-06 11:07:47','2018-07-06 11:07:47','dfa5c559-e51a-4ab0-9598-d3d6bf9946d4'),(161,3,46,NULL,3,1,'2018-07-06 11:07:47','2018-07-06 11:07:47','1fe96cf6-26e8-4992-9190-3c4cc4323af8'),(162,3,46,NULL,5,2,'2018-07-06 11:07:47','2018-07-06 11:07:47','6c3bc4f2-78de-449a-aae6-df0e5338279c'),(163,11,46,NULL,17,1,'2018-07-06 11:07:47','2018-07-06 11:07:47','8b659205-4a7f-4bca-b687-0725460e6d72'),(164,2,48,NULL,20,1,'2018-07-06 11:07:51','2018-07-06 11:07:51','658871c1-fcda-40f4-99f3-7a3bc2c6f73f'),(165,2,48,NULL,21,2,'2018-07-06 11:07:51','2018-07-06 11:07:51','e88c2f5b-8df9-49ce-8743-613d6267f7bb'),(166,2,48,NULL,22,3,'2018-07-06 11:07:51','2018-07-06 11:07:51','39869548-6ad1-48fb-8436-86494bf38272'),(167,3,48,NULL,3,1,'2018-07-06 11:07:51','2018-07-06 11:07:51','605ab836-cebd-4257-9c1c-d1eac62c040c'),(168,3,48,NULL,5,2,'2018-07-06 11:07:51','2018-07-06 11:07:51','3bf6db0e-e64c-4662-9559-cb2c1fad3f2d'),(169,11,48,NULL,17,1,'2018-07-06 11:07:51','2018-07-06 11:07:51','74a89c3d-3dc7-4b52-814b-d3420d83679d');
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resourcepaths`
--

DROP TABLE IF EXISTS `resourcepaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resourcepaths`
--

LOCK TABLES `resourcepaths` WRITE;
/*!40000 ALTER TABLE `resourcepaths` DISABLE KEYS */;
INSERT INTO `resourcepaths` VALUES ('1051a273','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\craftsupport\\dist'),('16d5c340','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\fabric'),('196f7fc6','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\d3'),('1de068a4','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\installer\\dist'),('22089940','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\craftsupport\\dist'),('249ff69a','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\xregexp'),('26d6e7b','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\matrix\\dist'),('2a8ea001','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\fields\\dist'),('2a92a23f','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\updater\\dist'),('2aa08433','C:\\xampp\\htdocs\\absolut-website\\vendor\\markhuot\\craftql\\src\\resources'),('2d588f0f','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\timepicker'),('2f7d0038','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\jquery-ui'),('2fdc9171','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\searchindexes\\dist'),('32af3fcc','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\element-resize-detector'),('35891df8','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\prismjs'),('39b48fee','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\jquery-ui'),('3bfb01e4','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\fileupload'),('3e296a30','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\recententries\\dist'),('3fd22eb2','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\routes\\dist'),('42fe3161','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\timepicker'),('48356173','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\tablesettings\\dist'),('48a23424','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\editcategory\\dist'),('4e5ce663','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\dashboard\\dist'),('4f05af57','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\jquery.payment'),('4f80f991','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\plugins\\dist'),('50e23938','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\velocity'),('545dbf8a','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\fileupload'),('54695466','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\pluginstore\\dist'),('5b9aa87a','@lib'),('62414c2e','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\element-resize-detector'),('62de51a5','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\picturefill'),('63b06982','@bower/jquery/dist'),('67794510','C:\\xampp\\htdocs\\craft\\vendor\\froala\\craft-froala-wysiwyg\\src\\assets\\field\\dist'),('68b2f671','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\utilities\\dist'),('6eb942bd','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\xregexp'),('6efa2463','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\editentry\\dist'),('75b382d9','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\jquery-touch-events'),('7a7cd1dd','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\updater\\dist'),('7e6c9bdb','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\jquery.payment'),('7fafa9df','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\prismjs'),('81cb521d','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\recententries\\dist'),('82744570','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\cp\\dist'),('87185474','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\dbbackup\\dist'),('8b867f43','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\feed\\dist'),('93285e','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\updateswidget\\dist'),('a88f7de3','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\selectize'),('b37650b3','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\generalsettings\\dist'),('b451718b','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\matrixsettings\\dist'),('b6e914ce','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\sites\\dist'),('b8a489a2','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\installer\\dist'),('ba015b0b','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\sites\\dist'),('bb5e0d20','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\generalsettings\\dist'),('be46f235','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\selectize'),('bf711073','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\updateswidget\\dist'),('c18c7e71','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\fields\\dist'),('c1e967b0','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\garnishjs'),('c3990bdb','C:\\xampp\\htdocs\\craft\\vendor\\froala\\wysiwyg-editor'),('c8bbbb44','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\picturefill'),('cbbec565','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\editentry\\dist'),('cc677578','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\login\\dist'),('cdf61777','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\utilities\\dist'),('d0de6b84','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\edituser\\dist'),('d4d0f0c2','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\routes\\dist'),('d53e844a','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\pluginstore\\dist'),('d5d28288','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\feed\\dist'),('d720e866','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\garnishjs'),('dfccb6bf','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\clearcaches\\dist'),('e378d16b','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\cp\\dist'),('e96fb00b','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\matrix\\dist'),('eb180765','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\dashboard\\dist'),('efc4a524','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\fabric'),('f131cfc6','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\src\\web\\assets\\matrixsettings\\dist'),('f3b2346b','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\deprecationerrors\\dist'),('f5a2aae7','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\velocity'),('f7d7595e','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\src\\web\\assets\\tablesettings\\dist'),('fe707310','C:\\xampp\\htdocs\\craft\\vendor\\craftcms\\cms\\lib\\d3'),('ffb747a1','C:\\xampp\\htdocs\\absolut-website\\vendor\\craftcms\\cms\\lib\\jquery-touch-events');
/*!40000 ALTER TABLE `resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) DEFAULT NULL,
  `uriParts` varchar(255) NOT NULL,
  `uriPattern` varchar(255) NOT NULL,
  `template` varchar(500) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `routes_uriPattern_idx` (`uriPattern`),
  KEY `routes_siteId_idx` (`siteId`),
  CONSTRAINT `routes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchindex`
--

DROP TABLE IF EXISTS `searchindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchindex`
--

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;
INSERT INTO `searchindex` VALUES (1,'username',0,1,' goodmorning '),(1,'firstname',0,1,''),(1,'lastname',0,1,''),(1,'fullname',0,1,''),(1,'email',0,1,' sandro goodmorning ge '),(1,'slug',0,1,''),(2,'slug',0,1,' bar '),(2,'title',0,1,' bar '),(3,'slug',0,1,' restaurant '),(3,'title',0,1,' restaurant '),(4,'slug',0,1,' hotel '),(4,'title',0,1,' hotel '),(5,'slug',0,1,' club '),(5,'title',0,1,' club '),(6,'slug',0,1,' hello '),(6,'title',0,1,' hello '),(7,'slug',0,1,' world '),(7,'title',0,1,' world '),(8,'slug',0,1,' s '),(8,'title',0,1,' s '),(9,'slug',0,1,' spirit '),(9,'title',0,1,' spirit '),(10,'slug',0,1,' wine '),(10,'title',0,1,' wine '),(11,'slug',0,1,' champagne '),(11,'title',0,1,' champagne '),(12,'field',1,1,' rival coctail bar '),(12,'field',2,1,' champagne spirit wine '),(12,'field',3,1,' restaurant hotel club '),(12,'field',6,1,' 2147483647 '),(12,'field',4,1,' https website com '),(12,'field',5,1,' email gmail com '),(12,'field',10,1,' gldanula tbilisi georgia '),(12,'field',7,1,' 35 2344 14 3423 '),(13,'field',8,1,' 14 3423 '),(13,'field',9,1,' 35 2344 '),(13,'slug',0,1,''),(12,'slug',0,1,' rival coctail bar '),(12,'title',0,1,' rival coctail bar '),(12,'field',11,1,' screenshot 2 screenshot 3 '),(14,'filename',0,1,' screenshot_3 png '),(14,'extension',0,1,' png '),(14,'kind',0,1,' image '),(14,'slug',0,1,''),(14,'title',0,1,' screenshot 3 '),(15,'filename',0,1,' screenshot_2 png '),(15,'extension',0,1,' png '),(15,'kind',0,1,' image '),(15,'slug',0,1,''),(15,'title',0,1,' screenshot 2 '),(16,'filename',0,1,' screenshot_4 png '),(16,'extension',0,1,' png '),(16,'kind',0,1,' image '),(16,'slug',0,1,''),(16,'title',0,1,' screenshot 4 '),(17,'filename',0,1,' screenshot_2_180702_102323 png '),(17,'extension',0,1,' png '),(17,'kind',0,1,' image '),(17,'slug',0,1,''),(17,'title',0,1,' screenshot 2 '),(18,'filename',0,1,' screenshot_3_180702_102323 png '),(18,'extension',0,1,' png '),(18,'kind',0,1,' image '),(18,'slug',0,1,''),(18,'title',0,1,' screenshot 3 '),(19,'field',1,1,' coca cola burger '),(19,'field',2,1,''),(19,'field',3,1,' bar restaurant hotel club '),(19,'field',11,1,''),(19,'field',6,1,' 321432 '),(19,'field',4,1,''),(19,'field',5,1,''),(19,'field',10,1,''),(19,'field',7,1,''),(19,'slug',0,1,' coca cola burger '),(19,'title',0,1,' coca cola burger '),(12,'field',12,1,''),(12,'field',13,1,''),(19,'field',12,1,''),(19,'field',13,1,''),(20,'slug',0,1,' burger '),(20,'title',0,1,' burger '),(21,'slug',0,1,' bar '),(21,'title',0,1,' bar '),(22,'slug',0,1,' cocktails '),(22,'title',0,1,' cocktails '),(23,'field',1,1,' burger bar '),(23,'field',2,1,' burger bar cocktails '),(23,'field',3,1,' restaurant club '),(23,'field',11,1,' screenshot 2 '),(23,'field',6,1,' 78328932 '),(23,'field',4,1,' http site com '),(23,'field',5,1,' mail hotmail com '),(23,'field',10,1,' georgia '),(23,'field',12,1,' tbilisi '),(23,'field',13,1,' saburtalo '),(23,'field',7,1,' 11 53 12 34 '),(24,'field',8,1,' 12 34 '),(24,'field',9,1,' 11 53 '),(24,'slug',0,1,''),(23,'slug',0,1,' burger bar '),(23,'title',0,1,' burger bar '),(25,'field',1,1,' hello '),(25,'field',2,1,''),(25,'field',3,1,''),(25,'field',11,1,''),(25,'field',6,1,' 342 '),(25,'field',4,1,''),(25,'field',5,1,''),(25,'field',10,1,''),(25,'field',12,1,''),(25,'field',13,1,''),(25,'field',7,1,''),(25,'slug',0,1,' hello '),(25,'title',0,1,' hello '),(12,'field',14,1,''),(19,'field',14,1,''),(23,'field',14,1,''),(25,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(26,'field',1,1,' hello '),(26,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(26,'field',2,1,''),(26,'field',3,1,''),(26,'field',11,1,''),(26,'field',6,1,' 342 '),(26,'field',4,1,''),(26,'field',5,1,''),(26,'field',10,1,''),(26,'field',12,1,''),(26,'field',13,1,''),(26,'field',7,1,''),(26,'slug',0,1,' hello 1 '),(26,'title',0,1,' hello '),(27,'field',1,1,' hello '),(27,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(27,'field',2,1,''),(27,'field',3,1,''),(27,'field',11,1,''),(27,'field',6,1,' 342 '),(27,'field',4,1,''),(27,'field',5,1,''),(27,'field',10,1,''),(27,'field',12,1,''),(27,'field',13,1,''),(27,'field',7,1,''),(27,'slug',0,1,' hello 2 '),(27,'title',0,1,' hello '),(28,'field',1,1,' hello '),(28,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(28,'field',2,1,''),(28,'field',3,1,''),(28,'field',11,1,''),(28,'field',6,1,' 342 '),(28,'field',4,1,''),(28,'field',5,1,''),(28,'field',10,1,''),(28,'field',12,1,''),(28,'field',13,1,''),(28,'field',7,1,''),(28,'slug',0,1,' hello 2 1 '),(28,'title',0,1,' hello '),(29,'field',1,1,' hello '),(29,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(29,'field',2,1,''),(29,'field',3,1,''),(29,'field',11,1,''),(29,'field',6,1,' 342 '),(29,'field',4,1,''),(29,'field',5,1,''),(29,'field',10,1,''),(29,'field',12,1,''),(29,'field',13,1,''),(29,'field',7,1,''),(29,'slug',0,1,' hello 2 1 1 '),(29,'title',0,1,' hello '),(30,'field',1,1,' hello '),(30,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(30,'field',2,1,''),(30,'field',3,1,''),(30,'field',11,1,''),(30,'field',6,1,' 342 '),(30,'field',4,1,''),(30,'field',5,1,''),(30,'field',10,1,''),(30,'field',12,1,''),(30,'field',13,1,''),(30,'field',7,1,''),(30,'slug',0,1,' hello 2 1 1 1 '),(30,'title',0,1,' hello '),(31,'field',1,1,' hello '),(31,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(31,'field',2,1,''),(31,'field',3,1,''),(31,'field',11,1,''),(31,'field',6,1,' 342 '),(31,'field',4,1,''),(31,'field',5,1,''),(31,'field',10,1,''),(31,'field',12,1,''),(31,'field',13,1,''),(31,'field',7,1,''),(31,'slug',0,1,' hello 2 1 1 1 1 '),(31,'title',0,1,' hello '),(32,'field',1,1,' hello '),(32,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(32,'field',2,1,''),(32,'field',3,1,''),(32,'field',11,1,''),(32,'field',6,1,' 342 '),(32,'field',4,1,''),(32,'field',5,1,''),(32,'field',10,1,''),(32,'field',12,1,''),(32,'field',13,1,''),(32,'field',7,1,''),(32,'slug',0,1,' hello 2 1 1 1 1 1 '),(32,'title',0,1,' hello '),(33,'field',1,1,' hello '),(33,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(33,'field',2,1,''),(33,'field',3,1,''),(33,'field',11,1,''),(33,'field',6,1,' 342 '),(33,'field',4,1,''),(33,'field',5,1,''),(33,'field',10,1,''),(33,'field',12,1,''),(33,'field',13,1,''),(33,'field',7,1,''),(33,'slug',0,1,' hello 2 1 1 1 1 1 1 '),(33,'title',0,1,' hello '),(34,'field',1,1,' hello '),(34,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(34,'field',2,1,''),(34,'field',3,1,''),(34,'field',11,1,''),(34,'field',6,1,' 342 '),(34,'field',4,1,''),(34,'field',5,1,''),(34,'field',10,1,''),(34,'field',12,1,''),(34,'field',13,1,''),(34,'field',7,1,''),(34,'slug',0,1,' hello 2 1 1 1 1 1 1 1 '),(34,'title',0,1,' hello '),(35,'field',1,1,' hello '),(35,'field',14,1,' fdskljfklsdaj kfldsjfkl sajfkldsajfkldsjfkldsal '),(35,'field',2,1,''),(35,'field',3,1,''),(35,'field',11,1,''),(35,'field',6,1,' 342 '),(35,'field',4,1,''),(35,'field',5,1,''),(35,'field',10,1,''),(35,'field',12,1,''),(35,'field',13,1,''),(35,'field',7,1,''),(35,'slug',0,1,' hello 2 1 1 1 1 1 1 1 1 '),(35,'title',0,1,' hello '),(36,'field',1,1,' burger bar '),(36,'field',14,1,''),(36,'field',2,1,' bar burger cocktails '),(36,'field',3,1,' restaurant club '),(36,'field',11,1,' screenshot 2 '),(36,'field',6,1,' 78328932 '),(36,'field',4,1,' http site com '),(36,'field',5,1,' mail hotmail com '),(36,'field',10,1,' georgia '),(36,'field',12,1,' tbilisi '),(36,'field',13,1,' saburtalo '),(36,'field',7,1,' 11 53 12 34 '),(36,'slug',0,1,' burger bar 1 '),(36,'title',0,1,' burger bar '),(37,'field',8,1,' 12 34 '),(37,'field',9,1,' 11 53 '),(37,'slug',0,1,''),(38,'field',1,1,' burger bar '),(38,'field',14,1,''),(38,'field',2,1,' bar burger cocktails '),(38,'field',3,1,' restaurant club '),(38,'field',11,1,' screenshot 2 '),(38,'field',6,1,' 78328932 '),(38,'field',4,1,' http site com '),(38,'field',5,1,' mail hotmail com '),(38,'field',10,1,' georgia '),(38,'field',12,1,' tbilisi '),(38,'field',13,1,' saburtalo '),(38,'field',7,1,' 11 53 12 34 '),(38,'slug',0,1,' burger bar 1 1 '),(38,'title',0,1,' burger bar '),(39,'field',8,1,' 12 34 '),(39,'field',9,1,' 11 53 '),(39,'slug',0,1,''),(40,'field',1,1,' burger bar '),(40,'field',14,1,''),(40,'field',2,1,' bar burger cocktails '),(40,'field',3,1,' restaurant club '),(40,'field',11,1,' screenshot 2 '),(40,'field',6,1,' 78328932 '),(40,'field',4,1,' http site com '),(40,'field',5,1,' mail hotmail com '),(40,'field',10,1,' georgia '),(40,'field',12,1,' tbilisi '),(40,'field',13,1,' saburtalo '),(40,'field',7,1,' 11 53 12 34 '),(40,'slug',0,1,' burger bar 1 1 1 '),(40,'title',0,1,' burger bar '),(41,'field',8,1,' 12 34 '),(41,'field',9,1,' 11 53 '),(41,'slug',0,1,''),(42,'field',1,1,' burger bar '),(42,'field',14,1,''),(42,'field',2,1,' bar burger cocktails '),(42,'field',3,1,' restaurant club '),(42,'field',11,1,' screenshot 2 '),(42,'field',6,1,' 78328932 '),(42,'field',4,1,' http site com '),(42,'field',5,1,' mail hotmail com '),(42,'field',10,1,' georgia '),(42,'field',12,1,' tbilisi '),(42,'field',13,1,' saburtalo '),(42,'field',7,1,' 11 53 12 34 '),(42,'slug',0,1,' burger bar 1 1 1 1 '),(42,'title',0,1,' burger bar '),(43,'field',8,1,' 12 34 '),(43,'field',9,1,' 11 53 '),(43,'slug',0,1,''),(44,'field',1,1,' burger bar '),(44,'field',14,1,''),(44,'field',2,1,' bar burger cocktails '),(44,'field',3,1,' restaurant club '),(44,'field',11,1,' screenshot 2 '),(44,'field',6,1,' 78328932 '),(44,'field',4,1,' http site com '),(44,'field',5,1,' mail hotmail com '),(44,'field',10,1,' georgia '),(44,'field',12,1,' tbilisi '),(44,'field',13,1,' saburtalo '),(44,'field',7,1,' 11 53 12 34 '),(44,'slug',0,1,' burger bar 1 1 1 1 1 '),(44,'title',0,1,' burger bar '),(45,'field',8,1,' 12 34 '),(45,'field',9,1,' 11 53 '),(45,'slug',0,1,''),(46,'field',1,1,' burger bar '),(46,'field',14,1,''),(46,'field',2,1,' bar burger cocktails '),(46,'field',3,1,' restaurant club '),(46,'field',11,1,' screenshot 2 '),(46,'field',6,1,' 78328932 '),(46,'field',4,1,' http site com '),(46,'field',5,1,' mail hotmail com '),(46,'field',10,1,' georgia '),(46,'field',12,1,' tbilisi '),(46,'field',13,1,' saburtalo '),(46,'field',7,1,' 11 53 12 34 '),(46,'slug',0,1,' burger bar 1 1 1 1 1 1 '),(46,'title',0,1,' burger bar '),(47,'field',8,1,' 12 34 '),(47,'field',9,1,' 11 53 '),(47,'slug',0,1,''),(48,'field',1,1,' burger bar '),(48,'field',14,1,''),(48,'field',2,1,' bar burger cocktails '),(48,'field',3,1,' restaurant club '),(48,'field',11,1,' screenshot 2 '),(48,'field',6,1,' 78328932 '),(48,'field',4,1,' http site com '),(48,'field',5,1,' mail hotmail com '),(48,'field',10,1,' georgia '),(48,'field',12,1,' tbilisi '),(48,'field',13,1,' saburtalo '),(48,'field',7,1,' 11 53 12 34 '),(48,'slug',0,1,' burger bar 1 1 1 1 1 1 1 '),(48,'title',0,1,' burger bar '),(49,'field',8,1,' 12 34 '),(49,'field',9,1,' 11 53 '),(49,'slug',0,1,'');
/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagateEntries` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_handle_unq_idx` (`handle`),
  UNIQUE KEY `sections_name_unq_idx` (`name`),
  KEY `sections_structureId_idx` (`structureId`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,NULL,'Properties','properties','channel',1,1,'2018-07-02 07:17:49','2018-07-02 07:18:17','5d374a8d-c95c-4fc0-94e4-5478d810295a');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections_sites`
--

DROP TABLE IF EXISTS `sections_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  KEY `sections_sites_siteId_idx` (`siteId`),
  CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections_sites`
--

LOCK TABLES `sections_sites` WRITE;
/*!40000 ALTER TABLE `sections_sites` DISABLE KEYS */;
INSERT INTO `sections_sites` VALUES (1,1,1,1,'property-details/{slug}','property-details/_entry',1,'2018-07-02 07:17:49','2018-07-02 07:18:17','9daf9764-0e9a-488f-a3da-6cec41e2f2e1');
/*!40000 ALTER TABLE `sections_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_idx` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,1,'iOt-WmMVwV9yq6YutxRbFKNIQYIVyq-7jy0OJvhMV3rNMtVef7E1NDsx_6cv5nF0k1Rv_2z0y9pyjPuKnH0PrdpTyCnqUWDIzuNZ','2018-07-02 12:48:40','2018-07-02 13:01:17','8eb37a0c-3431-469c-845e-dbce877d83cc'),(3,1,'rFli5bjkemNStNzrzj0xKQhkB9i76DxOWr9aQhHvkcqPo-dzgMenW_1-1nFMLqM8cjISSJ-T8QNQSaYRMkmZmpftc43QorHjtm-6','2018-07-06 11:06:03','2018-07-06 12:31:18','7a5c6e96-a41c-4900-8975-6fb0a7b77c99'),(4,1,'yVXYX6wUZ9L2MDkqappXTe_WEGnXoVVjSsFjm10E4li0yOXuSL4O7Snli7g1HOaAFLd-PzzP2CMu-7xuEn6QhN5BynQS9jnG3JUS','2018-07-07 12:04:45','2018-07-07 12:05:11','57d1fabc-c651-4d0f-adaf-0504315343fd');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shunnedmessages`
--

DROP TABLE IF EXISTS `shunnedmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shunnedmessages`
--

LOCK TABLES `shunnedmessages` WRITE;
/*!40000 ALTER TABLE `shunnedmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `shunnedmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitegroups`
--

DROP TABLE IF EXISTS `sitegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sitegroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitegroups`
--

LOCK TABLES `sitegroups` WRITE;
/*!40000 ALTER TABLE `sitegroups` DISABLE KEYS */;
INSERT INTO `sitegroups` VALUES (1,'Absolut','2018-07-02 06:38:30','2018-07-02 06:38:30','e0948488-a3b2-4277-a33d-6e1e15bd81c7');
/*!40000 ALTER TABLE `sitegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sites_handle_unq_idx` (`handle`),
  KEY `sites_sortOrder_idx` (`sortOrder`),
  KEY `sites_groupId_fk` (`groupId`),
  CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,1,1,'Absolut','default','en-US',1,'@web/',1,'2018-07-02 06:38:30','2018-07-02 06:38:30','13ed0bda-8756-4389-8cab-7be777651453');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structureelements`
--

DROP TABLE IF EXISTS `structureelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_idx` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structureelements`
--

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;
INSERT INTO `structureelements` VALUES (1,1,NULL,1,1,10,0,'2018-07-02 07:22:16','2018-07-02 07:22:39','748ee4ad-2673-426b-a653-846f16b1f885'),(2,1,2,1,2,3,1,'2018-07-02 07:22:16','2018-07-02 07:22:16','733f77c3-76f5-4db5-8263-b8b537567a83'),(3,1,3,1,4,5,1,'2018-07-02 07:22:24','2018-07-02 07:22:24','67e6e21b-4853-4ec6-a595-9745af6aa43c'),(4,1,4,1,6,7,1,'2018-07-02 07:22:33','2018-07-02 07:22:33','60cc8a4a-4d8a-46a0-a7f5-f034c0a6092c'),(5,1,5,1,8,9,1,'2018-07-02 07:22:39','2018-07-02 07:22:39','1df64379-f03c-44c5-aa8a-156438eef721');
/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
INSERT INTO `structures` VALUES (1,NULL,'2018-07-02 07:14:24','2018-07-02 07:22:05','4a22fc7f-a1f1-411f-ba7b-5da15575a2a4');
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemmessages`
--

DROP TABLE IF EXISTS `systemmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  KEY `systemmessages_language_idx` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemmessages`
--

LOCK TABLES `systemmessages` WRITE;
/*!40000 ALTER TABLE `systemmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemsettings`
--

DROP TABLE IF EXISTS `systemsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemsettings`
--

LOCK TABLES `systemsettings` WRITE;
/*!40000 ALTER TABLE `systemsettings` DISABLE KEYS */;
INSERT INTO `systemsettings` VALUES (1,'email','{\"fromEmail\":\"sandro@goodmorning.ge\",\"fromName\":\"Absolut\",\"transportType\":\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"}','2018-07-02 06:38:31','2018-07-02 06:38:31','551dab7c-4b1b-4abe-869e-a8039d2f27c9');
/*!40000 ALTER TABLE `systemsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggroups`
--

DROP TABLE IF EXISTS `taggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `taggroups_handle_unq_idx` (`handle`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggroups`
--

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;
INSERT INTO `taggroups` VALUES (1,'tags','tags',3,'2018-07-02 07:15:38','2018-07-02 07:24:27','46a42c5f-d3f1-4449-bb40-7cd3f3c3a655');
/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_idx` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (6,1,'2018-07-02 07:24:01','2018-07-02 07:24:01','447e889d-062b-4c48-b987-2138e32ac47d'),(7,1,'2018-07-02 07:24:05','2018-07-02 07:24:05','67819d2b-e2a5-4a7b-a8d3-866923463b0c'),(8,1,'2018-07-02 07:25:06','2018-07-02 07:25:06','8920fbb2-1ed9-476f-a796-9fa3f993fda9'),(9,1,'2018-07-02 07:25:14','2018-07-02 07:25:14','4af4d77c-5a2a-46d9-8f8f-ea9a8829ac5a'),(10,1,'2018-07-02 07:25:16','2018-07-02 07:25:16','c466bf86-f6cd-4fed-a54c-94972bbcb295'),(11,1,'2018-07-02 07:25:20','2018-07-02 07:25:20','495adbce-8ee0-4e6d-9628-09bf7ade1841'),(20,1,'2018-07-04 09:19:58','2018-07-04 09:19:58','cff84039-5908-4f79-9198-9c662d21cc5a'),(21,1,'2018-07-04 09:20:01','2018-07-04 09:20:01','fa651f4f-35ba-4fc8-818a-2b6d0690020b'),(22,1,'2018-07-04 09:20:11','2018-07-04 09:20:11','c663bf16-cdaa-469d-aac2-223c43abcc1f');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecacheelements`
--

DROP TABLE IF EXISTS `templatecacheelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  KEY `templatecacheelements_elementId_idx` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecacheelements`
--

LOCK TABLES `templatecacheelements` WRITE;
/*!40000 ALTER TABLE `templatecacheelements` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecacheelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecachequeries`
--

DROP TABLE IF EXISTS `templatecachequeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  KEY `templatecachequeries_type_idx` (`type`),
  CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecachequeries`
--

LOCK TABLES `templatecachequeries` WRITE;
/*!40000 ALTER TABLE `templatecachequeries` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecachequeries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecaches`
--

DROP TABLE IF EXISTS `templatecaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `templatecaches_siteId_idx` (`siteId`),
  CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecaches`
--

LOCK TABLES `templatecaches` WRITE;
/*!40000 ALTER TABLE `templatecaches` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  UNIQUE KEY `usergroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups_users`
--

DROP TABLE IF EXISTS `usergroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_idx` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups_users`
--

LOCK TABLES `usergroups_users` WRITE;
/*!40000 ALTER TABLE `usergroups_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions`
--

DROP TABLE IF EXISTS `userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions`
--

LOCK TABLES `userpermissions` WRITE;
/*!40000 ALTER TABLE `userpermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_usergroups`
--

DROP TABLE IF EXISTS `userpermissions_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_idx` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_usergroups`
--

LOCK TABLES `userpermissions_usergroups` WRITE;
/*!40000 ALTER TABLE `userpermissions_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_users`
--

DROP TABLE IF EXISTS `userpermissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_idx` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_users`
--

LOCK TABLES `userpermissions_users` WRITE;
/*!40000 ALTER TABLE `userpermissions_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpreferences`
--

DROP TABLE IF EXISTS `userpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text,
  PRIMARY KEY (`userId`),
  CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpreferences`
--

LOCK TABLES `userpreferences` WRITE;
/*!40000 ALTER TABLE `userpreferences` DISABLE KEYS */;
INSERT INTO `userpreferences` VALUES (1,'{\"language\":\"en-US\"}');
/*!40000 ALTER TABLE `userpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unq_idx` (`username`),
  UNIQUE KEY `users_email_unq_idx` (`email`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_photoId_fk` (`photoId`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'goodmorning',NULL,NULL,NULL,'sandro@goodmorning.ge','$2y$13$zRQCuXEG0UkNeSAlz5Z71uv1/2hx.Yhew8nh.NH19BtEkVsjMU4Bi',1,0,0,0,'2018-07-07 12:04:45','::1',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'2018-07-02 06:38:31','2018-07-02 06:38:31','2018-07-07 12:04:45','46b37ac9-30ad-47af-9635-6d89ded0eb80');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumefolders`
--

DROP TABLE IF EXISTS `volumefolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  KEY `volumefolders_parentId_idx` (`parentId`),
  KEY `volumefolders_volumeId_idx` (`volumeId`),
  CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumefolders`
--

LOCK TABLES `volumefolders` WRITE;
/*!40000 ALTER TABLE `volumefolders` DISABLE KEYS */;
INSERT INTO `volumefolders` VALUES (1,NULL,1,'Property Images','','2018-07-02 09:27:30','2018-07-02 09:27:30','5be3e40a-49ef-4986-8e00-be14cfd449ca'),(2,NULL,NULL,'Temporary source',NULL,'2018-07-02 09:28:11','2018-07-02 09:28:11','4001550c-0f44-488f-992d-ef2ca1b6b289'),(3,2,NULL,'user_1','user_1/','2018-07-02 09:28:11','2018-07-02 09:28:11','f5d97488-2d6f-4f37-8c3d-ad58847bdcbb'),(4,1,1,'rival-coctail-bar','rival-coctail-bar/','2018-07-02 09:30:53','2018-07-02 09:30:53','ccd24f1a-ab63-44a1-9897-55586c7dffbe');
/*!40000 ALTER TABLE `volumefolders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumes_name_unq_idx` (`name`),
  UNIQUE KEY `volumes_handle_unq_idx` (`handle`),
  KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
INSERT INTO `volumes` VALUES (1,5,'Property Images','propertyImages','craft\\volumes\\Local',1,'http://localhost/absolut-website/web/assets','{\"path\":\"assets\"}',1,'2018-07-02 09:27:30','2018-07-02 10:26:05','f7d2f635-0de5-4dcf-8219-a5a14ff12946');
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_idx` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,1,'craft\\widgets\\RecentEntries',1,0,'{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','cc104fc4-a4d5-4bb6-b087-b1fb1c524cb9'),(2,1,'craft\\widgets\\CraftSupport',2,0,'[]',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','8496541a-8695-400b-95d4-72acdd170875'),(3,1,'craft\\widgets\\Updates',3,0,'[]',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','f9d70378-cb2c-4b9a-abec-647b69ef8942'),(4,1,'craft\\widgets\\Feed',4,0,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2018-07-02 06:38:34','2018-07-02 06:38:34','576ebca0-58e3-4897-9d3b-562487d25daa');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-08 23:47:15
